import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import "package:flutter/material.dart";
import 'package:nft_market/Globals/Variables.dart' as variables;
import 'Globals/backend_config.dart';
import 'main.dart';
import "welcome.dart";
import 'package:http/http.dart' as http;

class login extends StatelessWidget {
  login({Key? key}) : super(key: key);
  TextEditingController usernameControl = TextEditingController();
  TextEditingController passwordControl = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: Stack (

              children: [
                AnimatedGradient(),
                Column (
                  children: [
                    Container(height: 100,),
                    Text("SU NFT", style: variables.loginFont),
                    Container(height: 6,),
                    Text("Discover all the NFTs of Sabancı University students!", style: variables.titleFontMedium,),
                    Container(height: 100),
                    Container(
                      width: variables.phoneSizeInfo.width*15/33 ,
                      child: TextFormField(
                        controller: usernameControl,
                        cursorColor: Colors.white,
                        maxLines: 1,
                        decoration: InputDecoration(
                          hintText: "Enter Username",
                          hintStyle: variables.assetFont,
                          enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                          focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                        ),
                        style: variables.scrollViewFont,
                      ),
                    ),
                    Container(height: 20),
                    Container(
                      width: variables.phoneSizeInfo.width*15/33 ,
                      child: TextFormField(
                        controller: passwordControl,
                        cursorColor: Colors.white,
                        maxLines: 1,
                        decoration: InputDecoration(
                          hintText: "Enter Password",
                          hintStyle: variables.assetFont,
                          enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                          focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                        ),
                        obscureText: true,
                        style: variables.scrollViewFont,
                      ),
                    ),
                    Spacer(),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Stack(
                        alignment: Alignment.bottomRight,
                        children: <Widget>[
                          Positioned.fill(
                            child: Container(
                              alignment: Alignment.bottomRight,
                              decoration: const BoxDecoration(
                                gradient: LinearGradient(
                                    colors: <Color>[
                                      Color(0xFF596EED),
                                      Color(0xFFED5CAB),
                                      //Color(0xFF42A5F5),
                                    ],
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight
                                ),
                              ),
                            ),
                          ),
                          CustomElevation(
                            height: 56,
                            child: TextButton(
                              style: TextButton.styleFrom(
                                shape: StadiumBorder(),
                                alignment: Alignment.bottomRight,
                                padding: const EdgeInsets.all(16.0),
                                primary: Colors.white,
                                textStyle: const TextStyle(fontSize: 20),
                              ),
                              onPressed:() async {
                                print("hereee");
                                var client = http.Client();

                                  final request2 = http.Request("POST", Uri.parse("https://mertd.pythonanywhere.com/auth/jwt/create/"));
                                  request2.headers.addAll(<String, String>{
                                  "Accept": "application/json",
                                  "Content-Type": "application/json",
                                  });
                                  request2.body = '{"username": "${usernameControl.text}" , "password": "${passwordControl.text}"}';

                                  final response = await request2.send();
                                  var x2 = await response.stream.bytesToString();

                                  print("x2 is $x2");

                                  print("hereee");

                                  variables.jwt = json.decode(x2);
                                  print(variables.jwt["access"]);
                                  var x = "JWT " + variables.jwt["access"];
                                  print(x);
                                final request3 = http.Request("GET", Uri.parse("https://mertd.pythonanywhere.com/auth/users/me/"));
                                request3.headers.addAll(<String, String>{
                                  "Accept": "application/json",
                                  "Content-Type": "application/json",
                                  "Authorization": x
                                });
                                final response3 = await request3.send();
                                var y = await response3.stream.bytesToString();
                                print(y);
                                variables.userDatas.add(userObject.fromJson(json.decode(y)));
                                Navigator.pop(context);
                                Navigator.pop(context);//yapıyoruz bu işi

                                    variables.Index = 0;


                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => MyHomePage(title: "title")),
                                  );



                            },
                              child: const Text('Log-in'),
                            ),
                          ),

                        ],
                      ),
                    ),
                    Container(height: 20),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Stack(
                        alignment: Alignment.bottomRight,
                        children: <Widget>[
                          Positioned.fill(
                            child: Container(
                              alignment: Alignment.bottomRight,
                              decoration: const BoxDecoration(
                                gradient: LinearGradient(
                                    colors: <Color>[
                                      Color(0xFF596EED),
                                      Color(0xFFED5CAB),
                                      //Color(0xFF42A5F5),
                                    ],
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight
                                ),
                              ),
                            ),
                          ),
                          CustomElevation(
                            height: 56,
                            child: TextButton(
                              style: TextButton.styleFrom(
                                shape: StadiumBorder(),
                                alignment: Alignment.bottomRight,
                                padding: const EdgeInsets.all(16.0),
                                primary: Colors.white,
                                textStyle: const TextStyle(fontSize: 20),
                              ),
                              onPressed: () {
                                Navigator.push(context, SlideRightRoute(page: welcome()));
                              },
                              child: const Text('Cancel'),
                            ),
                          ),

                        ],
                      ),
                    ),
                    Container(height: 70,),
                  ],
                ),
              ],
            )
        )
    );
  }
}

class SlideRightRoute extends PageRouteBuilder {
  final Widget page;
  SlideRightRoute({required this.page})
      : super(
    pageBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        ) =>
    page,
    transitionsBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child,
        ) =>
        SlideTransition(
          position: Tween<Offset>(
            begin: const Offset(-1, 0),
            end: Offset.zero,
          ).animate(animation),
          child: child,
        ),
  );
}

class CustomElevation extends StatelessWidget {
  final Widget child;
  final double height;

  CustomElevation({required this.child, required this.height})
      : assert(child != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(this.height / 2)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.blue.withOpacity(0.2),
            blurRadius: height / 5,
            offset: Offset(0, height / 5),
          ),
        ],
      ),
      child: this.child,
    );
  }
}

class AnimatedGradient extends StatefulWidget {
  @override
  _AnimatedGradientState createState() => _AnimatedGradientState();
}

class _AnimatedGradientState extends State<AnimatedGradient> {
  List<Color> colorList = [
    Colors.cyan.shade900,
    Colors.deepPurple.shade900,
    Colors.pink.shade900,
  ];
  List<Alignment> alignmentList = [
    Alignment.bottomLeft,
    Alignment.bottomRight,
    Alignment.topRight,
    Alignment.topLeft,
  ];
  int index = 0;
  Color bottomColor = Colors.pink.shade900;
  Color topColor = Colors.cyan.shade900;
  Alignment begin = Alignment.bottomLeft;
  Alignment end = Alignment.topRight;

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(milliseconds: 10), () {
      setState(() {
        bottomColor = Colors.deepPurple.shade900;
      });
    });
    return Scaffold(
        body: Stack(
          children: [
            AnimatedContainer(
              duration: Duration(seconds: 5),
              onEnd: () {
                setState(() {
                  index = index + 1;
                  // animate the color
                  bottomColor = colorList[index % colorList.length];
                  topColor = colorList[(index + 1) % colorList.length];

                  //// animate the alignment
                  // begin = alignmentList[index % alignmentList.length];
                  // end = alignmentList[(index + 2) % alignmentList.length];
                });
              },
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: begin, end: end, colors: [bottomColor, topColor])),
            ),
          ],
        ));
  }
}
