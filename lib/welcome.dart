import 'package:carousel_slider/carousel_slider.dart';
import "package:flutter/material.dart";
import 'package:nft_market/Globals/Variables.dart' as variables;
import "login.dart";
import "main.dart";

    class welcome extends StatefulWidget {
      const welcome({Key? key}) : super(key: key);

      @override
      State<welcome> createState() => _welcomeState();
    }

    class _welcomeState extends State<welcome> {
      @override
      Widget build(BuildContext context) {
        return Scaffold(
            body: Container(
                child: Stack (

                  children: [
                    AnimatedGradient(),
                    Column (
                      children: [
                        Container(height: 100,),
                        Text("SU NFT", style: variables.loginFont),
                        Container(height: 6,),
                        Text("Discover all the NFTs of Sabancı University students!", style: variables.titleFontMedium,),
                        Container(height: 100),
                        CarouselSlider(
                          options: CarouselOptions(

                            enlargeCenterPage: true,
                            enableInfiniteScroll: true,
                            autoPlay: true,
                          ),
                          items: variables.imageList.map((e) => ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Stack(
                              fit: StackFit.expand,
                              children: <Widget>[
                                Image.network(e,
                                  width: 1050,
                                  height: 350,
                                  fit: BoxFit.cover,)
                              ],
                            ) ,
                          )).toList(),
                        ),
                        Spacer(),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Stack(
                            alignment: Alignment.bottomRight,
                            children: <Widget>[
                              Positioned.fill(
                                child: Container(
                                  alignment: Alignment.bottomRight,
                                  decoration: const BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: <Color>[
                                          Color(0xFF596EED),
                                          Color(0xFFED5CAB),
                                          //Color(0xFF42A5F5),
                                        ],
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight
                                    ),
                                  ),
                                ),
                              ),
                              CustomElevation(
                                height: 56,
                                child: TextButton(
                                  style: TextButton.styleFrom(
                                    shape: StadiumBorder(),
                                    alignment: Alignment.bottomRight,
                                    padding: const EdgeInsets.all(16.0),
                                    primary: Colors.white,
                                    textStyle: const TextStyle(fontSize: 20),
                                  ),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => login()),
                                    );
                                  },
                                  child: const Text('Sign in'),
                                ),
                              ),

                            ],
                          ),
                        ),
                        Container(height: 30,),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Stack(
                            alignment: Alignment.bottomRight,
                            children: <Widget>[
                              Positioned.fill(
                                child: Container(
                                  alignment: Alignment.bottomRight,
                                  decoration: const BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: <Color>[
                                          Color(0xFF596EED),
                                          Color(0xFFED5CAB),
                                          //Color(0xFF42A5F5),
                                        ],
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight
                                    ),
                                  ),
                                ),
                              ),
                              CustomElevation(
                                height: 56,
                                child: TextButton(
                                  style: TextButton.styleFrom(
                                    shape: StadiumBorder(),
                                    alignment: Alignment.bottomRight,
                                    padding: const EdgeInsets.all(16.0),
                                    primary: Colors.white,
                                    textStyle: const TextStyle(fontSize: 20),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      variables.Index = 0;
                                    });
                                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                                        MyHomePage(title: "title")), (Route<dynamic> route) => false);
                                  },
                                  child: const Text('Browse without signing in'),
                                ),
                              ),

                            ],
                          ),
                        ),
                        Container(height: 10,),
                        Text("you need to sign in to buy/sell/create NFTs.", style: variables.titleFontMedium),
                        Container(height: 70,),
                      ],
                    ),
                  ],
                )

            )


        );
      }
    }


class CustomElevation extends StatelessWidget {
  final Widget child;
  final double height;

  CustomElevation({required this.child, required this.height})
      : assert(child != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(this.height / 2)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.blue.withOpacity(0.2),
            blurRadius: height / 5,
            offset: Offset(0, height / 5),
          ),
        ],
      ),
      child: this.child,
    );
  }
}

class AnimatedGradient extends StatefulWidget {
  @override
  _AnimatedGradientState createState() => _AnimatedGradientState();
}

class _AnimatedGradientState extends State<AnimatedGradient> {
  List<Color> colorList = [
    Colors.cyan.shade900,
    Colors.deepPurple.shade900,
    Colors.pink.shade900,
  ];
  List<Alignment> alignmentList = [
    Alignment.bottomLeft,
    Alignment.bottomRight,
    Alignment.topRight,
    Alignment.topLeft,
  ];
  int index = 0;
  Color bottomColor = Colors.pink.shade900;
  Color topColor = Colors.cyan.shade900;
  Alignment begin = Alignment.bottomLeft;
  Alignment end = Alignment.topRight;

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(milliseconds: 10), () {
      setState(() {
        bottomColor = Colors.deepPurple.shade900;
      });
    });
    return Scaffold(
        body: Stack(
          children: [
            AnimatedContainer(
              duration: Duration(seconds: 5),
              onEnd: () {
                setState(() {
                  index = index + 1;
                  // animate the color
                  bottomColor = colorList[index % colorList.length];
                  topColor = colorList[(index + 1) % colorList.length];

                  //// animate the alignment
                  // begin = alignmentList[index % alignmentList.length];
                  // end = alignmentList[(index + 2) % alignmentList.length];
                });
              },
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: begin, end: end, colors: [bottomColor, topColor])),
            ),
          ],
        ));
  }
}