
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nft_market/nft_page.dart';
import 'package:nft_market/Globals/Variables.dart' as variables;
import 'package:nft_market/Globals/backend_config.dart';
import "package:nft_market/Globals/backend_config.dart" as backendConf;
import 'dart:convert';
import 'package:http/http.dart' as http;
class nft_container_objects extends StatefulWidget {
  final NFTobject data;
  const nft_container_objects({Key? key, required this.data}) : super(key: key);

  @override
  _nft_container_objects createState() => _nft_container_objects();


}



class _nft_container_objects extends State<nft_container_objects>{
  
  late Icon heartobj;
  late bool like; //BACKEND DATA
  late int like_count;

  late List<userObject> likers;
  @override
  void initState() {
    super.initState();
    heartobj = Icon(CupertinoIcons.heart,size: 15,color: Colors.white);
    like_count = widget.data.likeCount;
    like = true;
    // TODO: implement initState
    getPrice();
  }
  String? price_;


  void likeNFT(String type) async{

    var client = http.Client();


    try {

      final request = http.Request(type, Uri.parse("https://mertd.pythonanywhere.com/api/favorites/"));
      request.headers.addAll(<String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
      });
      request.body = '{"UID": "${widget.data.address}" , "index": "${widget.data.nID}", "user": "${variables.userDatas[0].address}"}';

      await request.send();
      await getUserOwnedNFTs();

    } finally {
      client.close();
    }

    getFavoritedNFTs();
  }
  void getPrice() async{
    BigInt price = await backendConf.getSpecificPrice(widget.data.address,BigInt.from(widget.data.nID));
    var client = http.Client();

    try {
      print("ssdfkdjhg");
      print(widget.data.address);
      print(widget.data.nID);
      print(variables.userDatas);
      print(variables.userDatas[0].address);
      final response = (await client.get(
          Uri.https('mertd.pythonanywhere.com', "/api/favorites/" , {"UID": "${widget.data.address}" , "index": "${widget.data.nID}", "user": "${variables.userDatas[0].address}"})));

      print("skdjhg");
      final x = json.decode(utf8.decode(response.bodyBytes)).cast<Map<String, dynamic>>();
      print("merhaba");
      if(x.toString() != "[]"){

        setState(() {
          like = false;
          price_ = price.toString();
          heartobj = Icon(CupertinoIcons.heart_fill,size: 15,color: Colors.red,);
        });
      }
      else{

        setState(() {
          like = true;
          price_ = price.toString();

          heartobj =  Icon(CupertinoIcons.heart,size: 15,color: Colors.white);
        });
      }
    }
    catch(exception){
      setState(() {
        like = false;
        price_ = price.toString();
        heartobj = Icon(CupertinoIcons.heart_slash,size: 15,color: Colors.red,);
      });
    }
    finally {
      client.close();
    }



  }
  @override
  Widget build(BuildContext context){




    return GestureDetector(
      onTap: (()=>{
        Navigator.push(context, MaterialPageRoute(
            builder: (context) => nft_page(
              data: widget.data,
            )//BACKEND DATA
        ),
        )
      }
      ),//BACKEND DATA 'YA GÖRE YOL YAP
      child: Container(


        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(

          boxShadow: [BoxShadow(
            color: variables.forgroundColor,
            blurRadius: 10,


          ),],

          borderRadius: BorderRadius.circular(20),
          color: Colors.white10,


        ),
        width: 900,
        height: 261,

        child: Column(
          children: [

            Container(

                alignment: Alignment.topCenter,
                decoration: BoxDecoration(



                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  image: DecorationImage(alignment: Alignment.topCenter,image: NetworkImage(widget.data.dataLink)), //BACKEND DATA todo: make slug work
                ),
                width: 900,
                height: 178,

                margin: EdgeInsets.only(bottom: 10),


              ),

            Container(
                width: 180,
                height: 72.5,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
    begin: Alignment.topRight,
    end: Alignment.bottomLeft,
    colors: [
    variables.forgroundColor,
    variables.bottomNavBarColor,
    ],),

                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20)),
                  color: variables.textColor,


                ),

                child: Column(
                  children: [
                    Container(height: 5,),
                    Text(widget.data.name ?? "",style: variables.assetFont,),//BACKEND DATA
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(

                          children: [
                            Container(width: 10,),
                            Icon(CupertinoIcons.money_dollar, size: 20,color: Colors.white,),
                            Text(price_ ?? "-", style: variables.collectionFont,) //BACKEND DATA todo
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            IconButton(
                              icon: heartobj,
                              splashRadius: 1,
                              alignment: Alignment.centerRight,
                              onPressed: ()=>{

                                setState(() {
                                  if(like){
                                    heartobj = Icon(CupertinoIcons.heart_fill,size: 15,color: Colors.red,);
                                    like = false;
                                    like_count += 1;
                                    likeNFT("POST");


                                  }
                                  else{
                                    like = true;
                                    heartobj =  Icon(CupertinoIcons.heart,size: 15,color: Colors.white);
                                    like_count -= 1;
                                    likeNFT("DELETE");
                                  }


                                })
                              },
                            ), //BACKEND DATA heartfill or heart
                            Text("$like_count",style: variables.collectionFont),//BACKEND DATA todo
                            Container(width: 10,)
                          ],
                        ),





                      ],
                    ),
                  ],
                )


            ),

          ],
        ),

      ),
    );
  }
}