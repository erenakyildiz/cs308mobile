import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nft_market/Globals/Variables.dart' as variables;
import 'package:nft_market/Globals/backend_config.dart';
import 'package:nft_market/collection_page.dart';

class collection_object extends StatelessWidget{

  final NFTcollectionobject id;
  collection_object(this.id);
  @override
  Widget build(BuildContext context){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(

          onTap: (){
            Navigator.push(context, MaterialPageRoute(
                builder: (context) => Collection_page(
              collection_id: id,
            )

            ),
            );
          },

          child: Container(

            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              image:  DecorationImage(image: NetworkImage( "https://mertd.pythonanywhere.com" + (id.collectionImage ?? "/media/profilePictures/admin.jpg")) ),//BACKEND DATA
            ),
            alignment: Alignment.bottomCenter,
            child: Stack(

              children: [

                Positioned(
                  top:105,
                  child: Container(
                    alignment: Alignment.center,
                    width: 170,
                    height: 65,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                      color: variables.textColor,

                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 25),
                      child: Column(
                        children: [
                          Text(
                            id.name ?? "None",//BACKEND DATA
                            style: variables.scrollViewFont,
                          ),
                          Text(
                            id.owner ?? "No owner",
                            style: variables.titleFontSmall,
                          ),
                        ],
                      ),
                    ),

                  ),
                ),
                Positioned(

                  top:80,
                  left:60,
                  child: CircleAvatar(
                    radius: 25,

                    backgroundImage:  NetworkImage( "https://mertd.pythonanywhere.com" + (id.collectionImage ?? "/media/profilePictures/admin.jpg")),

                    backgroundColor: variables.forgroundColor, //BACKEND DATA
                  ),
                )
              ],
            ),
            height: 170.0,
            width: 170.0,

          ),

        ),

      ],
    );

  }
}