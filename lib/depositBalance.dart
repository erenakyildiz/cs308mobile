import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nft_market/Globals/backend_config.dart';
import 'package:search_page/search_page.dart';
import 'package:nft_market/nft_container_objects.dart';
import 'package:nft_market/Globals/Variables.dart' as variables;
import 'package:url_launcher/url_launcher.dart';
import 'package:nft_market/Globals/backend_config.dart';
/// This is a very simple class, used to
/// demo the `SearchPage` package


class depositBalance extends StatelessWidget {
  TextEditingController amount_deposit = TextEditingController();
  TextEditingController amount_withdraw = TextEditingController();

  //BACKEND DATA
  depositBalance();
  final balance = 0;


  void SendToMetamask_deposit() async{
    String amountToTransfer_deposit = amount_deposit.text;
    launch("https://wastera.com/_COIN_deposit.php?amount="+amountToTransfer_deposit);
      }
  void SendToMetamask_withdraw() async{
    String amountToTransfer_withdraw = amount_withdraw.text;
    launch("https://wastera.com/_COIN_withdraw.php?amount="+amountToTransfer_withdraw);
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: variables.backgroundColor,
      appBar: AppBar(
        backgroundColor: variables.buttonColor,
        title: Text("Balance transfer",style: variables.titleFontBig,),

      ),
      body: SingleChildScrollView(
        child: Container(


          child: Column(
            children: [

               Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(20),
                    padding: EdgeInsets.all(4),
                    alignment: Alignment.center,
                    height: 40,
                    decoration: BoxDecoration(
                      color: variables.forgroundColor,
                      borderRadius: BorderRadius.all(Radius.circular(10)),

                    ),
                    child: Text(
                      "Warning, you must have enough SUcoin in your wallet to be able to use this function",style: variables.userBalanceStyle,textAlign: TextAlign.center,
                    ),
                  ),
                  const SizedBox(
                    height: 100,
                  ),
                  Text(
                      "Your SUcoin balance: " + variables.userBalanceNotOnMarket.toString() + " SU\$",style: variables.titleFontBig,
                  ),
                ],
              ),

              Container(height: 30,),

              Container(
                width: variables.phoneSizeInfo.width*4/13 ,
                child: TextFormField(
                  controller: amount_deposit,
                  keyboardType: TextInputType.number,
                  cursorColor: Colors.white,
                  maxLines: 1,
                  decoration: InputDecoration(
                    hintText: "Enter Amount",
                    hintStyle: variables.assetFont,
                    enabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                    border: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                  ),
                  style: variables.scrollViewFont,

                ),
              ),
              Container(height: 30,),
              GestureDetector(
                onTap: ()=> SendToMetamask_deposit(),
                child: Container(
                  decoration: BoxDecoration(

                      color: variables.buttonColor,
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Send SUcoins ", style: variables.assetFont,),
                      const Icon(Icons.payments_outlined,color: Colors.white,size: 30,),
                    ],
                  ),

                  width: 200,
                  height: 50,

                ),
              ),
              const SizedBox(
                height: 50,
              ),
              Column(
                children: [

                  Text(
                    "Your market balance: " + variables.userBalanceOnMarket.toString() + " SU\$",style: variables.titleFontBig,
                  ),
                ],
              ),Container(height: 30,),
              Container(
                width: variables.phoneSizeInfo.width*4/13 ,
                child: TextFormField(
                  controller: amount_withdraw,
                  keyboardType: TextInputType.number,
                  cursorColor: Colors.white,
                  maxLines: 1,
                  decoration: InputDecoration(
                    hintText: "Enter Amount",
                    hintStyle: variables.assetFont,
                    enabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                    border: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                  ),
                  style: variables.scrollViewFont,

                ),
              ),
              Container(height: 30,),
              GestureDetector(
                onTap: ()=> SendToMetamask_withdraw(),
                child: Container(
                  decoration: BoxDecoration(

                      color: variables.buttonColor,
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Receive SUcoins ", style: variables.assetFont,),
                      Icon(Icons.payments_outlined,color: Colors.white,size: 30,),
                    ],
                  ),

                  width: 200,
                  height: 50,

                ),
              ),




            ],
          ),

        ),
      ),

    );
  }
}


