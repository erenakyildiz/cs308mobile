import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nft_market/Category.dart';
import 'package:nft_market/Globals/Variables.dart' as variables;
import 'package:nft_market/Search.dart';
import 'package:nft_market/collection_object.dart';
import 'package:nft_market/depositBalance.dart';
import 'package:nft_market/welcome.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:nft_market/collection_page.dart';
import 'package:search_page/search_page.dart';
import 'package:nft_market/Globals/backend_config.dart';
import 'package:nft_market/nft_container_objects.dart';
import 'package:http/http.dart' as http;
bool searchNFThelper = true;
IconData searchIcon = Icons.image;

String dropdownValue = 'One';
ScrollController profileScroll = ScrollController();
void main() {
  runApp(const MyApp());
}


Future<void> hello() async{
  await launch("https://metamask.app.link/dapp/wastera.com/paymentRequest.html");
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Flutter Demo',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.



      },
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  void _incrementCounter() async{
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;

    });
    hello();
  }
  Future loadNFTS() async{
    variables.allNfts = await getNFTs();
    await getSpesificNFT("0x7edDaB71D70a5E424567F1093aB958122f2D2671");
    await LoadEverything();
    followedCollection = await getFavoritedCollections();
  }
  TextEditingController imageControl = TextEditingController();
  TextEditingController nameControl = TextEditingController();
  TextEditingController descriptionControl = TextEditingController();
  TextEditingController idControl = TextEditingController();
  TextEditingController collectionController = TextEditingController();
  void SendToMetamaskForTransactions() async{
    String NftName = nameControl.text;
    String imageURL = imageControl.text;
    String description = descriptionControl.text;
    String id = idControl.text;
    String collectionName = dropdownValue;
    launch("https://metamask.app.link/dapp/wastera.com/_MINT_NFTS.php?name="+NftName+"&desc="+description+"&url="+imageURL+"&coll="+collectionName+"&id="+id);//MINTING !!!!
  }
  void SendToMetamaskForTransactionsNewCollection() async{
    String NftName = nameControl.text;
    String imageURL = imageControl.text;
    String description = descriptionControl.text;
    String id = idControl.text;
    String collectionName = collectionController.text;
    launch("https://metamask.app.link/dapp/wastera.com/_NFT_MINT.php?name="+NftName+"&desc="+description+"&url="+imageURL+"&coll="+collectionName+"&id="+id);//MINTING !!!!
  }

  late List<NFTcollectionobject> followedCollection;
  @override
  void initState(){
    // TODO: implement initState
    super.initState();
    loadNFTS();
    getFavoritedNFTs();
  }
  @override
  Widget build(BuildContext context) {

    loadNFTS();
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    if(variables.Index == 0) {
      return Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: variables.backgroundColor,
            appBar: AppBar(
              backgroundColor: variables.forgroundColor,

              title: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                  Image.asset("assets/images/sucoins.png", height: 30, width: 30,),
                    SizedBox(width: 10,),
                    Text("SUNFT",  style: variables.titleFontBig,
                    ),
                  ],
                ),
              ),

            ),
            body:RefreshIndicator(
              //get data from db in this refresh function
              onRefresh: () async {
                await Future.delayed(Duration(seconds: 1));//sleep for 1 sec for testing
              },
              child: ListView(
                children: [

                  const SizedBox(
                    height: 20,
                  ),
                  //categories
                  Container(
                    width: variables.phoneSizeInfo.width,
                    height: 130,
                    child: ListView(
                      physics: BouncingScrollPhysics(),
                      // This next line does the trick.
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        const SizedBox(width: 20,),
                        GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(
                                builder: (context) => Category_page(
                                collection_id: 0),
                            ),
                            );

                          },
                          child: Container(

                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: const DecorationImage(image: AssetImage("assets/images/music.jpg")),
                            ),
                            child: Align(
                              alignment: FractionalOffset.bottomLeft,

                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 12.0, left: 3.0),
                                child: Text(
                                    "Audio",
                                    style: variables.scrollViewFont
                                ),
                              ),


                            ),
                            width: 130.0,

                          ),
                        ),
                        const SizedBox(width: 20,),
                        GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(
                              builder: (context) => Category_page(
                                  collection_id: 1),
                            ),
                            );

                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: const DecorationImage(image: AssetImage("assets/images/art.jpg")),
                            ),
                            child: Align(
                              alignment: FractionalOffset.bottomLeft,

                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 12.0, left: 3.0),
                                child: Text(
                                    "İmage",
                                    style: variables.scrollViewFont
                                ),
                              ),


                            ),
                            width: 130.0,
                          ),
                        ),
                        const SizedBox(width: 20,),
                        GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(
                              builder: (context) => Category_page(
                                  collection_id: 2),
                            ),
                            );

                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: const DecorationImage(image: AssetImage("assets/images/photography.jfif")),
                            ),

                            child: Align(
                              alignment: FractionalOffset.bottomLeft,

                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 12.0, left: 3.0),
                                child: Text(
                                  "Video",
                                  style: variables.scrollViewFont
                                  ),
                              ),


                              ),

                            width: 130.0,
                            ),
                        ),


                      ],
                    ),
                  ),

                  const SizedBox(
                    height: 40,
                  ),
                  //Heading "notable drops" in opensea
                  Container(

                    child: Padding(
                      padding: const EdgeInsets.only(left: 30,),
                      child: Text(
                        "New NFT Collections",style: variables.titleFontBig,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: variables.phoneSizeInfo.width,
                    height: 200,
                    child: ListView(
                      physics: BouncingScrollPhysics(),
                      // This next line does the trick.
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        const SizedBox(width: 20,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(

                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => Collection_page(
                                  collection_id: variables.allNFTcollections[0],
                                )//BACKEND DATA
                                ),
                                );
                              },
                              child: Container(

                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  image: DecorationImage(image: NetworkImage("https://mertd.pythonanywhere.com" + (variables.allNFTcollections.isNotEmpty ? variables.allNFTcollections[0].collectionImage! : "/media/default.jpg"))),
                                ),
                                height: 170.0,
                                width: 170.0,

                              ),

                            ),
                            Text(variables.allNFTcollections.isNotEmpty ? variables.allNFTcollections[0].name! : "no name",style: variables.scrollViewFont, ),

                          ],
                        ),
                        const SizedBox(width: 20,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => Collection_page(
                                      collection_id: variables.allNFTcollections[1],
                                    )//BACKEND DATA
                                ),
                                );
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  image: DecorationImage(image:  NetworkImage( "https://mertd.pythonanywhere.com" + (variables.allNFTcollections.isNotEmpty ? variables.allNFTcollections[1].collectionImage! : "file:///media/media/default.jpg"))),
                                ),
                                height: 170.0,
                                width: 170.0,
                              ),
                            ),
                            Text(variables.allNFTcollections.isNotEmpty ? variables.allNFTcollections[1].name! : "no name",style: variables.scrollViewFont, ),
                          ],
                        ),
                        const SizedBox(width: 20,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => Collection_page(
                                      collection_id: variables.allNFTcollections[2],
                                    )//BACKEND DATA
                                ),
                                );
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  image: DecorationImage( image: NetworkImage( "https://mertd.pythonanywhere.com" + (variables.allNFTcollections.isNotEmpty ? variables.allNFTcollections[2].collectionImage! : "file:///media/media/default.jpg"))),
                                ),


                                height: 170.0,
                                width: 170.0,
                              ),
                            ),
                            Text(variables.allNFTcollections.isNotEmpty ? variables.allNFTcollections[2].name! : "no name",style: variables.scrollViewFont, ),
                          ],
                        ),


                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  Container(

                    child: Padding(
                      padding: const EdgeInsets.only(left: 30,),
                      child: Text(
                        "Trending Collections",style: variables.titleFontBig,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: variables.phoneSizeInfo.width,
                    height: 200,
                    child: ListView(
                      physics: BouncingScrollPhysics(),
                      // This next line does the trick.
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        const SizedBox(width: 20,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(

                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => Collection_page(
                                      collection_id: variables.hotNFTCollections[0],
                                    )//BACKEND DATA
                                ),
                                );
                              },
                              child: Container(

                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  image: DecorationImage(image: NetworkImage( "https://mertd.pythonanywhere.com" + (variables.hotNFTCollections.isNotEmpty ? variables.hotNFTCollections[0].collectionImage! : "file:///media/media/default.jpg"))),
                                ),
                                height: 170.0,
                                width: 170.0,

                              ),

                            ),
                            Text(variables.hotNFTCollections.isNotEmpty ? variables.hotNFTCollections[0].name! : "no name",style: variables.scrollViewFont, ),

                          ],
                        ),
                        const SizedBox(width: 20,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => Collection_page(
                                      collection_id: variables.hotNFTCollections[1],
                                    )//BACKEND DATA
                                ),
                                );
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  image: DecorationImage(image:  NetworkImage( "https://mertd.pythonanywhere.com" + (variables.hotNFTCollections.isNotEmpty ? variables.hotNFTCollections[1].collectionImage! : "file:///media/media/default.jpg"))),
                                ),
                                height: 170.0,
                                width: 170.0,
                              ),
                            ),
                            Text(variables.hotNFTCollections.isNotEmpty ? variables.hotNFTCollections[1].name! : "no name",style: variables.scrollViewFont, ),
                          ],
                        ),
                        const SizedBox(width: 20,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => Collection_page(
                                      collection_id: variables.hotNFTCollections[2],
                                    )//BACKEND DATA
                                ),
                                );
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  image: DecorationImage( image: NetworkImage( "https://mertd.pythonanywhere.com" + (variables.hotNFTCollections.isNotEmpty ? variables.hotNFTCollections[2].collectionImage! : "file:///media/media/default.jpg"))),
                                ),


                                height: 170.0,
                                width: 170.0,
                              ),
                            ),
                            Text(variables.hotNFTCollections.isNotEmpty ? variables.hotNFTCollections[2].name! : "no name",style: variables.scrollViewFont, ),
                          ],
                        ),


                      ],
                    ),
                  ),

                ],
              ),


              color: Colors.purple,
            ),
            bottomNavigationBar: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
            backgroundColor: variables.bottomNavBarColor,
            currentIndex: variables.Index,
            selectedItemColor: variables.buttonColor,
            unselectedItemColor: variables.bottomNavBarUnselected,
            onTap: (index) =>
                setState(() => variables.Index = index
                ),
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
              BottomNavigationBarItem(icon: Icon(Icons.search), label: "Search"),
              BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile"),
              BottomNavigationBarItem(icon: Icon(Icons.create), label: "Create")
            ],
          ), // This trailing comma makes auto-formatting nicer for build methods.
          );
    }
    else if (variables.Index == 1) {
      if(searchNFThelper){
        return Scaffold(
          floatingActionButton: FloatingActionButton(
            heroTag: "btn2",
            onPressed: ()=>{
              setState(()=>{
                searchNFThelper = !searchNFThelper,
                if(searchIcon == Icons.image){
                  searchIcon = Icons.person

                }
                else{

                  searchIcon = Icons.image
                }

              })
            },
            backgroundColor: Colors.white54,
            child: Icon(
                searchIcon
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
          body: searching_page("Search For Users",variables.userDatas_),
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: variables.bottomNavBarColor,
            type: BottomNavigationBarType.fixed,
            currentIndex: variables.Index,
            selectedItemColor: variables.buttonColor,
            unselectedItemColor: variables.bottomNavBarUnselected,
            onTap: (index) =>
                setState(() => variables.Index = index
                ),
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
              BottomNavigationBarItem(icon: Icon(Icons.search), label: "Search"),
              BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile"),
              BottomNavigationBarItem(icon: Icon(Icons.create), label: "Create"),
            ],
          ), // This trailing comma makes auto-formatting nicer for build methods.
        );
      }
      else{
        return Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: ()=>{
              setState(()=>{
                searchNFThelper = !searchNFThelper,
                if(searchIcon == Icons.person){

                  searchIcon = Icons.image
                }
                else{

                  searchIcon = Icons.person
                }

              })
            },
            backgroundColor: Colors.white54,
            child: Icon(
                searchIcon
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
          body: searching_page_NFT(variables.allNfts),
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: variables.bottomNavBarColor,
            type: BottomNavigationBarType.fixed,
            currentIndex: variables.Index,
            selectedItemColor: variables.buttonColor,
            unselectedItemColor: variables.bottomNavBarUnselected,
            onTap: (index) =>
                setState(() => variables.Index = index
                ),
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
              BottomNavigationBarItem(icon: Icon(Icons.search), label: "Search"),
              BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile"),
              BottomNavigationBarItem(icon: Icon(Icons.create), label: "Create"),
            ],
          ), // This trailing comma makes auto-formatting nicer for build methods.
        );
      }

    }
    else if(variables.Index == 2){

      if (variables.userDatas.isEmpty) {
        return welcome();
      }
      else {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: variables.backgroundColor,
          body: Stack(
            children: [
              Container(
                height: variables.phoneSizeInfo.height * 0.2,
                width: variables.phoneSizeInfo.width,
                color: variables.backgroundColor,
                child: Center(
                  child: Column(
                    children: [
                      Container(height: variables.phoneSizeInfo.height * 0.03),
                      Padding(
                        padding: EdgeInsets.all(5),
                        child: CircleAvatar(

                          backgroundImage: NetworkImage((variables.userDatas[0]
                              .profilePicture ??
                              "https://upload.wikimedia.org/wikipedia/commons/f/f4/User_Avatar_2.png")),
                          radius: variables.phoneSizeInfo.width * 0.05,
                          backgroundColor: Colors.white,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                        child: Text(variables.userDatas[0].name ?? "Loading",
                          style: variables.titleFontBig,
                        ),
                      ),
                      Text(variables.userDatas[0].address,
                        style: variables.titleFontSmall,
                      ),
                      SizedBox(height: 10,),
                      Container(
                        width: 300,


                        alignment: Alignment.center,
                        height: 45,
                        decoration: BoxDecoration(
                          color: variables.buttonColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text("Market balance: " +
                                variables.userBalanceOnMarket.toString() + "\$",
                              style: variables.userBalanceStyle,),
                            GestureDetector(
                              onTap: (() =>
                              {
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => depositBalance()
                                ) //BACKEND DATA
                                )
                              }),

                              child: Column(

                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [

                                  Image(
                                    color: Colors.white,

                                    width: 30,
                                    image:
                                    AssetImage("assets/images/depositCoin.png"),

                                  ),
                                  Text("Deposit-Withdraw",
                                    style: variables.collectionFont,),
                                ],
                              ),
                            ),
                          ],
                        ),

                      ),

                    ],
                  ),
                ),
              ),
              DraggableScrollableSheet(
                initialChildSize: 0.6,
                minChildSize: 0.6,
                maxChildSize: 1,

                builder: (context, profileScroll) {
                  return Container(

                    decoration: BoxDecoration
                      (

                        color: variables.bottomNavBarColor,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            topLeft: Radius.circular(10))
                    ),
                    child: SingleChildScrollView(

                      physics: BouncingScrollPhysics(),
                      controller: profileScroll,
                      child: Column(

                        children: [

                          SizedBox(height: 10,),
                          Row(
                            children: [
                              Expanded(
                                child: GestureDetector(

                                  child: Container(
                                    margin: EdgeInsets.only(
                                        left: 10, right: 10),
                                    height: 30,
                                    decoration: BoxDecoration(
                                      color: variables.backgroundColor,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10)),
                                    ),
                                    child: Center(
                                      child: Text(
                                        "Watch list",
                                        style: variables.userBalanceStyle,
                                      ),
                                    ),
                                  ),
                                  onTap: () =>
                                  {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) =>
                                            searching_page_NFT_collections(
                                                followedCollection
                                            )
                                    )
                                    )
                                  },
                                ),
                              ),
                              Expanded(
                                child: GestureDetector(

                                  child: Container(
                                    margin: EdgeInsets.only(
                                        left: 10, right: 10),

                                    height: 30,


                                    decoration: BoxDecoration(
                                      color: variables.backgroundColor,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10)),
                                    ),
                                    child: Center(
                                      child: Text(
                                        "Liked NFTs",
                                        style: variables.userBalanceStyle,
                                      ),
                                    ),
                                  ),
                                  onTap: () =>
                                  {
                                    getFavoritedNFTs(),
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) =>
                                            searching_page_NFT(
                                                variables.favorites
                                            )
                                    ),),
                                  },
                                ),
                              )
                            ],
                          ),
                          Container(
                            height: 30,
                            margin: EdgeInsets.all(20),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Images", style: variables.titleFontBig,),
                          ),
                          Container(
                            height: 281,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: variables.userOwnedNFTS[0].length,
                              itemBuilder: (context, index) {
                                final NFTobject person = variables
                                    .userOwnedNFTS[0][index]; //0 is image
                                return Container(
                                  width: 200,
                                  height: 100,
                                  child: nft_container_objects(
                                      data: person), //todo make iterative
                                );
                              },

                            ),
                          ),
                          Container(
                            height: 30,
                            margin: EdgeInsets.all(20),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Videos", style: variables.titleFontBig,),
                          ),
                          Container(
                            height: 281,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: variables.userOwnedNFTS[1].length,
                              itemBuilder: (context, index) {
                                final NFTobject person = variables
                                    .userOwnedNFTS[1][index]; // 1 is video
                                return Container(
                                  width: 200,
                                  height: 100,
                                  child: nft_container_objects(
                                      data: person), //todo make iterative
                                );
                              },

                            ),
                          ),
                          Container(
                            height: 30,
                            margin: EdgeInsets.all(20),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Audios", style: variables.titleFontBig,),
                          ),
                          Container(
                            height: 281,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: variables.userOwnedNFTS[2].length,
                              itemBuilder: (context, index) {
                                final NFTobject person = variables
                                    .userOwnedNFTS[2][index]; //2 is audio
                                return Container(
                                  width: 200,
                                  height: 100,
                                  child: nft_container_objects(
                                      data: person), //todo make iterative
                                );
                              },

                            ),
                          ),
                          SizedBox(height: 150,),
                          Container(

                            decoration: BoxDecoration(

                              color: variables.forgroundColor,
                              borderRadius: BorderRadius.circular(10),

                            ),
                            height: 50,
                            width: 300,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Made by Eren Akyıldız and Barış Ulaş Çukur with ",
                                  style: variables.collectionFont,
                                ),
                                Icon(
                                  CupertinoIcons.heart_fill, color: Colors.red,)
                              ],
                            ),
                          )

                        ],

                      ),
                    ),
                  );
                },
              ),
            ],
          ),

          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: variables.bottomNavBarColor,
            currentIndex: variables.Index,
            selectedItemColor: variables.buttonColor,
            unselectedItemColor: variables.bottomNavBarUnselected,
            onTap: (index) =>
                setState(() {
                  variables.Index = index;
                  LoadEverything();
                }),
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.search), label: "Search"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person), label: "Profile"),
              BottomNavigationBarItem(icon: Icon(Icons.create), label: "Create")
            ],
          ), // This trailing comma makes auto-formatting nicer for build methods.
        );
      }
    }



    else{
      return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: variables.backgroundColor,
        body: Container(
          child: Column(
            children: [
              Container(height: 100,),
              Container(
                width: variables.phoneSizeInfo.width*4/13 ,
                child: TextFormField(
                  controller: imageControl,

                  cursorColor: Colors.white,
                  maxLines: 1,
                  decoration: InputDecoration(
                    hintText: "Enter URL",
                    hintStyle: variables.assetFont,
                    enabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                    border: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                  ),
                  style: variables.scrollViewFont,

                ),
              ),
              Container(height: 30,),

              Container(
                width: variables.phoneSizeInfo.width*4/13 ,
                child: TextFormField(
                  controller: nameControl,

                  cursorColor: Colors.white,
                  maxLines: 1,
                  maxLength: 20,


                  decoration: InputDecoration(
                    counterStyle: TextStyle(color:Colors.white),
                    hintText: "Enter NFT name",
                    hintStyle: variables.assetFont,
                    enabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                    border: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                  ),
                  style: variables.scrollViewFont,

                ),
              ),
              Container(height: 30,),
              Container(
                width: variables.phoneSizeInfo.width*4/13 ,
                child: TextFormField(
                  controller: idControl,

                  cursorColor: Colors.white,
                  maxLines: 1,


                  decoration: InputDecoration(
                    hintText: "Enter NFT ID",
                    hintStyle: variables.assetFont,
                    enabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                    border: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                  ),
                  style: variables.scrollViewFont,

                ),
              ),
              Container(height: 30,),
              Container(
                width: variables.phoneSizeInfo.width*4/13 ,

                child: TextFormField(
                  controller: descriptionControl,

                  cursorColor: Colors.white,

                  maxLines: 4,
                  decoration: InputDecoration(
                    hintText: "Enter description",
                    hintStyle: variables.collectionFont,

                    enabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                    border: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                  ),
                  style: variables.collectionFont,

                ),
              ),
          Container(
            width: 300,
            child: DropdownButton<String>(
              borderRadius: BorderRadius.circular(10),

              value: dropdownValue,
              icon: const Icon(Icons.arrow_downward),
              elevation: 16,
              style: TextStyle(color: variables.buttonColor),
              underline: Container(
                height: 2,
                color: variables.buttonColor,
              ),
              onChanged: (String? newValue) {
                setState(() {
                  dropdownValue = newValue!;
                });
              },
              isExpanded: true,
              menuMaxHeight: 250,
              items: <String>['One', 'Two', 'Free', 'Four','five',"siz","sev","egi","nin","NEWCOLLECTION"] //BACKEND DATA
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          ),
              Visibility(
                child: Container(

                  width: variables.phoneSizeInfo.width*4/13 ,
                  child: TextFormField(
                    controller: collectionController,
                    cursorColor: Colors.white,
                    style: variables.scrollViewFont,
                    maxLines: 1,
                    decoration: InputDecoration(
                      hintText: "Enter Collection name",
                      hintStyle: variables.collectionFont,

                      enabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                      focusedBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                      border: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    ),

                  ),
                ),
                visible: (dropdownValue == "NEWCOLLECTION"),
              ),


              SizedBox(height: 20,),
              GestureDetector(
                onTap: ()=> {
                  if(dropdownValue == "NEWCOLLECTION"){

                    SendToMetamaskForTransactionsNewCollection(),
                }
                  else{
                  SendToMetamaskForTransactions(),
                }

                },

                child: Container(
                  decoration: BoxDecoration(

                      color: variables.buttonColor,
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Create NFT ", style: variables.assetFont,),
                      Icon(Icons.add_circle_outline_rounded,color: Colors.white,size: 30,),
                    ],
                  ),

                  width: 200,
                  height: 50,
                  
                ),
              ),


            ],
          ),
          width: variables.phoneSizeInfo.width,
          height: variables.phoneSizeInfo.height,
        ),

        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: variables.bottomNavBarColor,
          currentIndex: variables.Index,
          selectedItemColor: variables.buttonColor,
          unselectedItemColor: variables.bottomNavBarUnselected,
          onTap: (index) =>
              setState(() => variables.Index = index
              ),
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
            BottomNavigationBarItem(icon: Icon(Icons.search), label: "Search"),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile"),
            BottomNavigationBarItem(icon: Icon(Icons.create), label: "Create")
          ],
        ),

      );
    }
  }
}
