import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:nft_market/startAuction.dart';
import 'dart:math'; //used for the random number generator
import "dart:io";
import 'package:path_provider/path_provider.dart';
import "dart:convert";
import 'package:nft_market/Globals/Variables.dart' as variables;
import 'package:charts_flutter/flutter.dart' as charts;
import "package:nft_market/collection_page.dart" as collection;
import "package:nft_market/Search.dart";
import 'package:url_launcher/url_launcher.dart';
import "nft_container_objects.dart" as nft_container_obj;
import "package:nft_market/Globals/backend_config.dart";
import "package:http/http.dart" as http;
class nft_page extends StatefulWidget {
  NFTobject data;
  nft_page({Key? key, required this.data}) : super(key: key);


  @override
  _nft_page createState() => _nft_page();


}
class SalesData {
  final int year;
  final int sales;

  SalesData(this.year, this.sales);
}



class _nft_page extends State<nft_page> {
  late List<userObject> likers;
  late double size;
  var somevar;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    size = 400.0;
    getLikers();
// get nft's likers
  }
  void getLikers() async{
    var client = http.Client();
    getsomethingidk();
    try {
      final response = (await client.get(
          Uri.http('10.0.2.2:8000', "/api/favorites/" , {"UID": "${widget.data.address}" , "index": "${widget.data.nID}"})));


      final items = json.decode(utf8.decode(response.bodyBytes)).cast<Map<String, dynamic>>();

      setState(() {
        likers = items.map<userObject>((json) {
          return userObject.fromJson(json);
        }).toList();
      });

    } finally {
      client.close();
    }





  }
  void getsomethingidk()async{
    var client = http.Client();

    try {
      final response = (await client.get(
      Uri.https('mertd.pythonanywhere.com', "/api/transactionHistory" , {"id": "${widget.data.PRIMARYKEY}"})));



      print(widget.data.PRIMARYKEY);
      somevar = json.decode(utf8.decode(response.bodyBytes)).cast<Map<String, dynamic>>();

      print(somevar);

    } finally {
    client.close();
    }
  }
  @override
  Widget build(BuildContext context) {

    ScrollController scr = ScrollController();
    final data = [
      SalesData(0, 10),
      SalesData(1, 20),
       SalesData(2, 35),
       SalesData(3, 30),
       SalesData(4, 21),
       SalesData(5, 18),
       SalesData(6, 13),
       SalesData(7, 21),
       SalesData(8, 20),
       SalesData(9, 12),
       SalesData(10, 20),
       SalesData(11, 24),
       SalesData(12, 20),
       SalesData(13, 2),
       SalesData(14, 20),
       SalesData(15, 3),
       SalesData(16, 12),
       SalesData(17, 9),
       SalesData(18, 11),
       SalesData(19, 30),
      SalesData(20, 45),
      SalesData(21, 100),
      SalesData(22, 99),
      SalesData(23, 90),
      SalesData(24, 110),
    ];

    _getSeriesData() {
      List<charts.Series<SalesData, int>> series = [
        charts.Series(
            id: "Sales",
            data: data,
            domainFn: (SalesData series, _) => series.year,
            measureFn: (SalesData series, _) => series.sales,
            colorFn: (SalesData series, _) => charts.MaterialPalette.blue.shadeDefault
        )
      ];
      return series;
    }

    return Scaffold(
      backgroundColor: variables.backgroundColor,
      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
      floatingActionButton: FloatingActionButton.small(
          child: Icon(Icons.arrow_back_rounded, color: Colors.white,),
          backgroundColor: variables.backgroundColor,
          onPressed: ()=>{
            Navigator.pop(context),
          }
      ),
      body: NotificationListener<ScrollNotification>(
        onNotification: (scrollNotification) {
          setState(() {
            if (scrollNotification.metrics.pixels > 0) {

              if(400 -scrollNotification.metrics.pixels *1.5 < 100){
                size = 100;

              }

              else {
                size = 400 - scrollNotification.metrics.pixels * 1.5;

              }


            }
          });
          return true;
        },
        child: Column(
          children: [

            Container(
              width: variables.phoneSizeInfo.width,
              height: size*3/4,
              color: variables.buttonColor,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      width: size*3/4,
                      height: size*7/15,



                      decoration: BoxDecoration(
                        boxShadow:  [
                          BoxShadow(
                            color: Colors.white.withOpacity(0.5),
                            spreadRadius: 10,
                            blurRadius: 7,
                            offset: Offset(0, 0), // changes position of shadow
                          ),
                        ],
                        image:
                        DecorationImage(image: NetworkImage(widget.data.dataLink)),
                          color: variables.backgroundColor,
                        borderRadius: BorderRadius.all(Radius.circular(20))
                      ),
                    ),
                  ),






                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                padding: EdgeInsets.all(10),
                alignment: Alignment.topLeft,
                height: size / 4 + 15,
                child: Wrap(
                  direction: Axis.horizontal,

                  crossAxisAlignment: WrapCrossAlignment.start,
                  children: [
                    Text(widget.data.name ?? "unavailable" , style: variables.scrollViewFont), //BACKEND DATA
                    Container(height: 20,),
                    Text(widget.data.description ?? "unavailable", style: variables.titleFontSmall,),//BACKEND DATA
                    Container(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          onTap: (()=>{
                            Navigator.push(context, MaterialPageRoute(
                                builder: (context) => searching_page("like",likers
                                )//BACKEND DATA
                            ),),
                          }),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text("Total likes" , style: variables.collectionFont,),
                              Row(
                                children: [
                                  Text(widget.data.likeCount.toString(),style: variables.collectionFont,),

                                  Icon(CupertinoIcons.heart_fill , color: Colors.white,size: 15,),

                                ],
                              ), //BACKEND DATA
                            ],
                          ),
                        ),


          if(widget.data.marketStatus == 2)...[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [

                            Text("Last bid" , style: variables.collectionFont,),
                            Row(
                              children: [
                                Text("123",style: variables.collectionFont,),

                                Icon(CupertinoIcons.money_dollar , color: Colors.white,size: 15,),
                              ],
                            ), //BACKEND DATA


                          ],
                        ),


]
                      ],
                    )

                  ],
                ),
              ),
            ), Container(
              width: 400,
              height: 110,
              margin: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: variables.bottomNavBarColor,
                borderRadius: BorderRadius.all(Radius.circular(20)),



              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("Contract ID: "+ widget.data.address, style: variables.userBalanceStyle,textAlign: TextAlign.center,),
                  Text("Token ID: " + widget.data.nID.toString(),style: variables.userBalanceStyle),
                  Text("Collection Name: "+ (widget.data.collectionName ?? "none"),style: variables.userBalanceStyle ),

                ],
              ),

            ),

             //SEARCH BAR
            Expanded(

              child: Container(
                color: variables.backgroundColor,
                child: GridView.count(

                  crossAxisCount: 1,
                  crossAxisSpacing: 10,
                  childAspectRatio: 1,
                  physics: BouncingScrollPhysics(),
                  children: [
                   Column(
                     children: [
                       if(widget.data.marketStatus == 2) ...[
                         Container(
                         margin: EdgeInsets.all(10),
                         decoration: BoxDecoration(
                           borderRadius: BorderRadius.all(Radius.circular(10)),
                           color: variables.buttonColor,
                         ),
                         child: ListTile(

                           dense: true,
                           title: Text(
                             "On auction", style: variables.assetFont,),//BACKEND DATA
                           subtitle: Text("Current bid: 123",style: variables.collectionFont), //BACKEND DATA

                             trailing: Container(
                               width: 200,
                               child: Row(
                                 children: [
                                   Container(
                                     width: 150,
                                     height: 44,
                                     child: TextFormField(
                                       cursorColor: Colors.white,
                                       textAlign: TextAlign.start,

                                       textAlignVertical: TextAlignVertical.center,
                                       decoration: InputDecoration(
                                         hintText: "Enter bid..",
                                         hintStyle: variables.collectionFont,
                                         focusedBorder: OutlineInputBorder(
                                           borderSide: BorderSide(width: 3, color: Colors.white),
                                           borderRadius: BorderRadius.all(Radius.circular(10)),
                                         ),

                                         enabledBorder: OutlineInputBorder(
                                           borderSide: BorderSide(width: 1, color: Colors.white),
                                           borderRadius: BorderRadius.all(Radius.circular(10)),
                                         ),

                                       ),
                                       style: TextStyle(
                                         color: Colors.white
                                       ),

                                       keyboardType: TextInputType.number,

                                     ),
                                   ),
                                   SizedBox(width: 15,),
                                   GestureDetector(
                                     onTap: () =>{
                                       //GO TO METAMASK WITH DATA
                                     },

                                     child: Column(

                                       children: [

                                         Image(
                                           color: Colors.white,

                                           width: 30,
                                           image:
                                         AssetImage("assets/images/auction.png"),

                                         ),
                                         Text("Bid",style: variables.collectionFont,),
                                       ],
                                     ),
                                   ),
                                 ],
                               ),
                             )

                         ),
                       ),
                       ]
                       else if(widget.data.marketStatus == 1)...[
                         Container(
                           margin: EdgeInsets.all(20),
                           padding: EdgeInsets.all(20),
                           decoration: BoxDecoration(
                             borderRadius: BorderRadius.all(Radius.circular(10)),
                             color: variables.buttonColor,
                           ),
                           child: Container(

                               child: Column(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: [
                                   Text(
                                     "Currently, this item is not for sale. You can withdraw it if you are the owner, or start an auction", style: variables.assetFont,textAlign: TextAlign.center,),

                                   Container(
                                     child: Row(
                                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                       children: [
                                         GestureDetector(
                                           onTap: () =>{
                                             //GO TO METAMASK WITH DATA
                                             launch("https://metamask.app.link/dapp/wastera.com/_NFTwithdraw.php?address=" + widget.data.address + "&id=" + widget.data.nID.toString())//DEPOSIT AND APPROVAL

                                           },

                                           child: Container(
                                             width: 80,
                                             decoration: BoxDecoration(

                                               color: variables.bottomNavBarColor,
                                               borderRadius: BorderRadius.all(Radius.circular(10)),
                                               boxShadow: [BoxShadow(
                                                 color: Colors.white,
                                                 blurRadius: 10,

                                               ),BoxShadow(
                                                 color: variables.buttonColor,
                                                 blurRadius: 10,

                                               )],
                                             ),
                                             child: Column(
                                               mainAxisAlignment: MainAxisAlignment.center,

                                               children: [

                                                 Image(
                                                   color: Colors.white,

                                                   width: 30,
                                                   image:
                                                   AssetImage("assets/images/withdraw.png"),

                                                 ),
                                                 Text("Withdraw",style: variables.collectionFont,),
                                               ],
                                             ),
                                           ),
                                         ),
                                         GestureDetector(
                                           onTap: () =>{
                                             //GO TO AUCTION PAGE
                                             Navigator.push(context, MaterialPageRoute(
                                                 builder: (context) => startAuction(widget.data.address,widget.data.nID.toString())
                                             )//BACKEND DATA
                                             )
                                           },

                                           child: Container(
                                             width: 80,
                                             decoration: BoxDecoration(

                                               color: variables.bottomNavBarColor,
                                               borderRadius: BorderRadius.all(Radius.circular(10)),
                                               boxShadow: [BoxShadow(
                                                 color: Colors.white,
                                                 blurRadius: 10,

                                               ),BoxShadow(
                                                 color: variables.buttonColor,
                                                 blurRadius: 10,

                                               )],
                                             ),
                                             child: Column(
                                               mainAxisAlignment: MainAxisAlignment.center,

                                               children: [

                                                 Image(
                                                   color: Colors.white,

                                                   width: 30,
                                                   image:
                                                   AssetImage("assets/images/auction.png"),

                                                 ),
                                                 Text("Start Auction",style: variables.collectionFont,),
                                               ],
                                             ),
                                           ),
                                         ),
                                       ],
                                     ),
                                   ),
                                 ],
                               ),//BACKEND DATA



                           ),
                         ),
                       ]
                       else ...[

                           Container(
                             margin: EdgeInsets.all(10),
                             decoration: BoxDecoration(
                               borderRadius: BorderRadius.all(Radius.circular(10)),
                               color: variables.buttonColor,
                             ),
                             child: ListTile(


                                 dense: true,
                                 title: Text(
                                   "This item is currently not on the market, Deposit it on the market ?", style: variables.assetFont,textAlign:  TextAlign.center,),//BACKEND DATA
                                  trailing: GestureDetector(
                                    onTap: () =>{
                                      //GO TO METAMASK WITH DATA
                                    launch("https://metamask.app.link/dapp/wastera.com/_NFTdeposit.php?address=" + widget.data.address + "&id=" + widget.data.nID.toString())//DEPOSIT AND APPROVAL

                                  },

                                    child: Container(
                                      height: 40,
                                      width: 80,
                                      decoration: BoxDecoration(

                                        color: variables.bottomNavBarColor,
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        boxShadow: [BoxShadow(
                                          color: Colors.white,
                                          blurRadius: 10,

                                        ),BoxShadow(
                                          color: variables.buttonColor,
                                          blurRadius: 10,

                                        )],
                                      ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,

                                        children: [

                                          Image(
                                            color: Colors.white,

                                            width: 20,
                                            image:
                                            AssetImage("assets/images/depositIcon.png"),

                                          ),
                                          Text("Deposit",style: variables.collectionFont,),
                                        ],
                                      ),
                                    ),
                                  ),



                             ),
                           ),
                       ],

                       Container(
                         padding: EdgeInsets.only(top: 30),
                         width: variables.phoneSizeInfo.width - 20,
                         child: ListTile(

                           dense: true,
                           leading: Image(image: AssetImage("assets/images/music.jpg")), //BACKEND DATA
                           title: Text("Created by ${widget.data.creatorName}", style: variables.assetFont,),//BACKEND DATA



                         ),
                       ),
                       SizedBox(height: 20,),
                       Container(
                         width: variables.phoneSizeInfo.width - 20,
                         child: ListTile(

                           dense: true,
                           leading: Image(image: AssetImage("assets/images/music.jpg")), //BACKEND DATA
                           title: Text("Owned by ${widget.data.currentOwner}", style: variables.assetFont,),//BACKEND DATA



                         ),
                       ),
                       Container(
                         width: variables.phoneSizeInfo.width - 20,
                         child: ListTile(

                           dense: true,
                           title: Text("About collection", style: variables.scrollViewFont,),//BACKEND DATA

                         ),
                       ),
                       Container(
                         width: variables.phoneSizeInfo.width -20,
                         padding: EdgeInsets.only(right: 10,left: 20),
                         child: Text(widget.data.collectionName ?? ""
                             ,style: variables.collectionFont,
                         ),
                       ),
                     ],
                   ),
                    //BACKEND DATA

                    Column(
                      children: [
                        Container(
                          width: variables.phoneSizeInfo.width - 20,
                          child: ListTile(
                            dense: true,
                            title: Text("Price history:", style: variables.scrollViewFont,),//BACKEND DATA

                          ),
                        ),
                        Expanded(

                          child:  charts.LineChart(_getSeriesData()),),

                      ],
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: variables.bottomNavBarColor,
                        borderRadius: BorderRadius.all(Radius.circular(20)),


                      ),
                      child: Column(
                        children: [

                          Text("Transaction History: ", style: variables.titleFontBig,),
                          if(somevar.toString() != "[]" && somevar != null)...{
                            Row(

                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("1)", style: variables.titleFontMedium),
                                Text(somevar[0]["oldOwner"], style: variables.titleFontMedium,),
                                Icon(Icons.arrow_forward_sharp, color: Colors.white54),

                                Text(somevar[0]["newOwner"], style: variables.titleFontMedium,),

                              ],
                            ),
                            Row(

                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(" \$: "+somevar[0]["price"].toString(), style: variables.titleFontMedium,),
                                Text(" Date: "+somevar[0]["time"].toString(), style: variables.titleFontMedium,),
                              ],
                            )
                          }else...{
                               Text("Nothing to show here", style: variables.titleFontBig)
                            }




                        ],
                      ),

                    ),
                    Column(
                      children: [

                        Container(
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.topLeft,
                          child:
                          Text(
                            "More from this collection", style: variables.scrollViewFont,
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),

                        Row(
                           children: [
                             Expanded(child: nft_container_obj.nft_container_objects(data: variables.allNfts[0],)), // todo koleksiyona göre gösterme yap
                           ],


                        ),

                      ],

                    )



                  ],

                ),

              ),
            ),
          ],
        ),
      ),//BACKEND DATA NEEDED
    );
  }
}

