import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nft_market/Globals/backend_config.dart';
import 'package:search_page/search_page.dart';
import 'package:nft_market/nft_container_objects.dart';
import 'package:nft_market/Globals/Variables.dart' as variables;
import 'package:url_launcher/url_launcher.dart';
import 'package:nft_market/Globals/backend_config.dart';
/// This is a very simple class, used to
/// demo the `SearchPage` package


class startAuction extends StatelessWidget {
  TextEditingController amount_deposit = TextEditingController();
  TextEditingController nft_id = TextEditingController();
  TextEditingController nft_address = TextEditingController();
  final String nftAddress;
  final String nftId;

  //BACKEND DATA
  startAuction(this.nftAddress,this.nftId){
    nft_id.text = this.nftId;
    nft_address.text = this.nftAddress;
  }
  final balance = 0;


  void StartAuctionFromMetamask() async{
    String amountToTransfer = amount_deposit.text;
    launch("https://wastera.com/_NFTstartAuction.php?amount="+amountToTransfer+"&id="+nftId+"&address="+nftAddress);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: variables.backgroundColor,
      appBar: AppBar(
        backgroundColor: variables.buttonColor,
        title: Text("Balance transfer",style: variables.titleFontBig,),

      ),
      body: SingleChildScrollView(
        child: Container(


          child: Column(
            children: [



              Container(height: 30,),

              Container(
                width: variables.phoneSizeInfo.width*4/13 ,
                child: TextFormField(
                  controller: amount_deposit,
                  keyboardType: TextInputType.number,
                  cursorColor: Colors.white,
                  maxLines: 1,
                  decoration: InputDecoration(
                    hintText: "Enter starting bid",
                    hintStyle: variables.assetFont,
                    enabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    disabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),

                    focusedBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                    border: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                  ),
                  style: variables.scrollViewFont,

                ),
              ),SizedBox(height: 10,),
              Container(
                width: variables.phoneSizeInfo.width*4/13 ,
                child: TextFormField(
                  controller: nft_address,
                  enabled: false,
                  cursorColor: Colors.white,
                  maxLines: 1,
                  decoration: InputDecoration(

                    hintStyle: variables.assetFont,
                    enabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    disabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                    border: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                  ),
                  style: TextStyle(color: Colors.white12),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                width: variables.phoneSizeInfo.width*4/13 ,
                child: TextFormField(
                  controller: nft_id,
                  enabled: false,
                  cursorColor: Colors.white,
                  maxLines: 1,
                  decoration: InputDecoration(
                    hintText: "Enter Amount",
                    hintStyle: variables.assetFont,
                    enabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    disabledBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: variables.buttonColor)),
                    border: OutlineInputBorder(  borderRadius: BorderRadius.all(Radius.circular(10)),borderSide: BorderSide(color: Colors.white)),
                  ),
                  style: TextStyle(color: Colors.white12),
                ),
              ),
              Container(height: 30,),
              GestureDetector(
                onTap: ()=> StartAuctionFromMetamask(),
                child: Container(
                  decoration: BoxDecoration(

                      color: variables.buttonColor,
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text("Start auction", style: variables.assetFont,),
                      const Image(image: AssetImage("assets/images/auction.png"),color: Colors.white,width: 40,)
                    ],
                  ),

                  width: 200,
                  height: 50,

                ),
              ),








            ],
          ),

        ),
      ),

    );
  }
}


