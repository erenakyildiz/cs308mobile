import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:nft_market/Globals/backend_config.dart';
import 'package:nft_market/collection_object.dart';
import 'dart:math'; //used for the random number generator
import "dart:io";
import 'package:path_provider/path_provider.dart';
import "dart:convert";
import 'package:nft_market/Globals/Variables.dart' as variables;
import 'package:nft_market/nft_page.dart';
import 'package:nft_market/Search.dart';
import 'package:nft_market/nft_container_objects.dart';
import "package:http/http.dart" as http;

class Collection_page extends StatefulWidget {
  final NFTcollectionobject collection_id;
  const Collection_page({Key? key, required this.collection_id}) : super(key: key);


  @override
  _Collection_page createState() => _Collection_page();


}




class _Collection_page extends State<Collection_page> {
  late String name_ascend;
  late String highlow;
  late double size;
  late List<userObject> nftCollectionSomething;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    size = 400.0;
    name_ascend = "A->Z";
    highlow = "Lowest first";
    help();
  }

  void help() async{
    nftCollectionSomething =await getCollectionFavouriters(widget.collection_id.name ?? "sorry");
  }
  void addtodbsomething() async{
    var client = http.Client();

    try {
      final request2 = http.Request("POST", Uri.parse("https://mertd.pythonanywhere.com/api/watchLists/"));
      request2.headers.addAll(<String, String>{
        "Accept": "application/json",
        "Content-Type": "application/json",
      });

      request2.body = '{"user": "${variables.userDatas[0].address}" ,"nftCollection": "${widget.collection_id.name}"}';
      print(request2.body);
      await request2.send();





    } finally {
      client.close();
    }

  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: variables.backgroundColor,

      floatingActionButtonLocation: FloatingActionButtonLocation.centerTop,
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FloatingActionButton.small(
                child: Icon(Icons.arrow_back_rounded, color: Colors.white,),
                backgroundColor: variables.backgroundColor,
                heroTag: "btn2",
                onPressed: ()=>{
                  Navigator.pop(context),
                }
            ),
            FloatingActionButton.small(
                child: Icon(CupertinoIcons.sort_down, color: Colors.white,),
                backgroundColor: variables.backgroundColor,
                heroTag: "btn1",
                onPressed: ()=>{
                 showDialog(context: context, builder: (context) {
                   return StatefulBuilder(builder: (context,setState){
                     return AlertDialog(
                       content: Container(
                         height: 100,
                         child: Column(
                           children: [
                             Row(
                               mainAxisAlignment: MainAxisAlignment.start,
                               children: [
                                 TextButton(
                                   style: TextButton.styleFrom(
                                     padding: const EdgeInsets.all(6.0),
                                     primary: Colors.white,
                                     textStyle: const TextStyle(fontSize: 20),
                                     side: BorderSide(color: Colors.white)
                                   ),
                                   child: Text("Name", style: variables.assetFont,),onPressed: ()=>{
                                   setState(() => {
                                     if(name_ascend == "A->Z") {
                                       name_ascend = "Z->A"
                                     }
                                     else{
                                       name_ascend = "A->Z"
                                     }
                                   }
                                   ),
                                 },
                                 ),
                                 Text("  " +name_ascend, style: variables.assetFont,)
                               ],
                             ),
                             Row(
                               mainAxisAlignment: MainAxisAlignment.start,
                               children: [
                                 TextButton(style: TextButton.styleFrom(
                                     padding: const EdgeInsets.all(6.0),
                                     primary: Colors.white,
                                     textStyle: const TextStyle(fontSize: 20),
                                     side: BorderSide(color: Colors.white)
                                 ),child: Text("Price", style: variables.assetFont,),onPressed: () =>
                                     setState(() => {
                                       if(highlow == "Lowest first"){
                                         highlow = "Highest first"
                                       }
                                       else{
                                         highlow = "Lowest first"
                                       }
                                     }
                                     ),),
                                 Text("  " + highlow, style: variables.assetFont,),
                               ],
                             ),
                           ],
                         ),
                       ),
                       title: Text("Sort items", style: variables.titleFontBig),
                       backgroundColor: variables.buttonColor,
                     );
                   }
                   );
                }
                 ),
                }
            ),
          ],
        ),
      ),

        body: NotificationListener<ScrollNotification>(
          onNotification: (scrollNotification) {
            setState(() {
              if (scrollNotification.metrics.pixels > 0) {

                if(400 -scrollNotification.metrics.pixels *1.5 < 100){
                  size = 100;

                }

                else {
                  size = 400 - scrollNotification.metrics.pixels * 1.5;

                }


              }
            });
            return true;
          },
          child: Column(
            children: [
              Container(
                width: variables.phoneSizeInfo.width,
                height: size*3/4,
                decoration: BoxDecoration(

                  color: variables.buttonColor,
                  image: DecorationImage(image: NetworkImage( "https://mertd.pythonanywhere.com" + (this.widget.collection_id.collectionImage ?? "/media/profilePictures/admin.jpg")),)
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 215-size/2, bottom: 10),
                      child: CircleAvatar(

                        radius: size/10 + 20,

                        backgroundColor: Colors.black,
                        backgroundImage: NetworkImage( "https://mertd.pythonanywhere.com" + (this.widget.collection_id.collectionImage ?? "/media/profilePictures/admin.jpg")),
                      ),
                    ),






                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  padding: EdgeInsets.all(10),
                  alignment: Alignment.topLeft,
                  height: size / 4 + 15,
                  child: Wrap(
                    direction: Axis.horizontal,

                    crossAxisAlignment: WrapCrossAlignment.start,
                    children: [
                      Text(this.widget.collection_id.name ?? "None", style: variables.scrollViewFont), //BACKEND DATA
                      Container(height: 20,),
                      Text(this.widget.collection_id.description ?? "None", style: variables.titleFontSmall,),//BACKEND DATA
                      Container(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(

                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text("Total likes",style: variables.collectionFont),
                                Text(this.widget.collection_id.numLikes.toString(),style: variables.collectionFont,), //BACKEND DATA
                                Icon(CupertinoIcons.heart_fill, size: 15,color: Colors.white),

                              ],
                            ),
                            onTap: (()=>{
                            Navigator.push(context, MaterialPageRoute(
                            builder: (context) => searching_page("like",nftCollectionSomething
                            )//BACKEND DATA
                            ),),
                            }),
                          ),
                          GestureDetector(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text("Total holders" , style: variables.collectionFont,),
                                Text("Not implemented",style: variables.collectionFont,), //BACKEND DATA
                                Icon(CupertinoIcons.person_2_fill , color: Colors.white,size: 15,),

                              ],
                            ),
                            onTap: (()=>{
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => searching_page("holder",variables.userDatas
                                  )//BACKEND DATA
                              ),),
                            }),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text("Floor price" , style: variables.collectionFont,),
                              Text("Not implemented",style: variables.collectionFont,), //BACKEND DATA
                              Icon(CupertinoIcons.money_dollar , color: Colors.white,size: 15,),

                            ],
                          ),



                        ],
                      )
                    ],
                  ),
                ),
              ),//DESC NAME ETC.
              GestureDetector(
                child: Container(
                  width:120,height: 30,
                  alignment: Alignment.center,

                  decoration: BoxDecoration(
                    color: variables.bottomNavBarColor,
                    borderRadius: BorderRadius.all(Radius.circular(20)),

                  ),
                  child: Text("Add to watchlist",style: variables.userBalanceStyle,),
                ),
                onTap: ()=>{
                  addtodbsomething(),
                },
              ), //SEARCH BAR
              Expanded(

                child: Container(
                  color: variables.backgroundColor,
                  child: GridView.count(

                    childAspectRatio: 0.7,
                    crossAxisCount: 2,

                    physics: BouncingScrollPhysics(),
                    children: [
                      nft_container_objects(data: variables.allNfts[0]),




                    ],),
                ),
              ),
            ],
          ),
        ),//BACKEND DATA NEEDED
    );
  }
}
