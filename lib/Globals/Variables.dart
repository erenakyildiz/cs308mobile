import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';
import 'package:nft_market/Globals/backend_config.dart';

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

final Color backgroundColor = HexColor.fromHex('#1A1A40');
final Color forgroundColor = HexColor.fromHex('#7A0BC0');
final Color textColor = HexColor.fromHex('#7a4fbf');
final Color buttonColor = HexColor.fromHex('#FA58B6');
final Color bottomNavBarColor = HexColor.fromHex('#0f0f21');
final Color bottomNavBarUnselected = Colors.white;


final titleFontBig = GoogleFonts.roboto(color: Colors.white70, fontSize: 20, fontWeight: FontWeight.w500,);
final titleFontSmall = GoogleFonts.roboto(color: Colors.white70, fontSize: 10, fontWeight: FontWeight.w300,);
final titleFontMedium = GoogleFonts.roboto(color: Colors.white70, fontSize: 15, fontWeight: FontWeight.w300,);
final userBalanceStyle = GoogleFonts.roboto(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w400 );
final scrollViewFont = GoogleFonts.roboto(color: Colors.white, fontSize: 20, fontWeight: FontWeight.w500,);
final loginFont = GoogleFonts.roboto(color: Colors.white, fontSize: 30, fontWeight: FontWeight.w900);
final assetFont = GoogleFonts.roboto(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500,);
final collectionFont = GoogleFonts.roboto(color: Colors.white, fontSize: 12,fontWeight: FontWeight.w400);

final List<String> imageList = [
  "https://lh3.googleusercontent.com/c5rFrzgsa4EIMKSujwvwu3i_yHgoGWyegPfjN4uw3TAxjmagCV2L9I3qsJm1VYKTtK4pLYP9W8b6ReODEMK1RIbffTRYWcJshsbl=w600",
  "https://lh3.googleusercontent.com/eo_OEbYFUD87dGay7MKsxz1Grz2Qz9_-HsWBwrotRLHJgScIIjOZudKXHePeabeZFIy7kHYyBo0w29AYpXW2YhydZJkgo-E69rgp=w600",
  "https://lh3.googleusercontent.com/PR4tFYvWZniWIBQEfaUQ5n0xZO4g71fHuD_ad4DwfgSUHRcmIRFq866qEtV1m8j68vf64Zh_hEb04Acm9p8roMpKnoU11c0yc7zX7Q=w600",
  "https://yourimageshare.com/ib/6YmPpunOh5.png",
  "https://lh3.googleusercontent.com/4VOmNoE782Yl0BcF0kpN7vwS9xLW3yIAk3Q2rDHfqo3lBW-IdbhuTAUJTHJnNbidjjCcO8aVF7WiYVjWlTXecq_yxmaj9DhgnpL21Q=w600",
];


final Size phoneSizeInfo = window.physicalSize;

final List<String> images = ["assets/images/asset1.jpg","assets/images/asset2.webp"];
final List<String> assetCategories = ["Collected Assets", "Favorites", "Art", "Music", "Sports", "Collectibles"];
// asset categories will later be connected to backend to get the data, I've just refactored the code to make it ready.
var Index = 0;


//SMART CONTRACT OBJECTS
var nftMarketContract;
var suCoinContract;
var web3Client;

var userBalanceNotOnMarket;
var userBalanceOnMarket;
List<tokenObject> tokenDatas = [];
List<userObject> userDatas = [];
List<NFTobject> allNfts =[];
List<categories> allCategories = [];
List<List<NFTobject>> userOwnedNFTS = [[],[],[]];
List<List<NFTobject>> allNftsByCategory = [[],[],[]];
List<NFTobject> favorites =[];
List<NFTcollectionobject> allNFTcollections = [];
List<NFTcollectionobject> hotNFTCollections = [];
List<userObject> userDatas_ = [];

var jwt ;