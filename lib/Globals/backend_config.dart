import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:nft_market/collection_object.dart';
import 'package:web3dart/web3dart.dart';
import 'package:nft_market/Globals/Variables.dart' as variables;
//get NFT's
//http://localhost:8000/api/nfts?format=json
//nft object
class categories{
  final String name;
  final String? pic1;
  final String? pic2;
  categories({required this.name,required this.pic1, required this.pic2});
  factory categories.fromJson(Map<String, dynamic> json){
    return categories(
      name : json["name"],
      pic1: json["backgroundImage"],
      pic2: json["foregroundImage"]
    );

  }Map<String, dynamic> toJson() => {
    'pic2' : pic2,
    'pic1': pic1,
    'name': name,
  };
}

class NFTobject {
  final int PRIMARYKEY;
  final String address;
  final int nID;
  final String? name;
  final String? description;
  final String metaDataType;
  final String dataLink;
  final String? collectionName;
  final String? creatorName;
  final String currentOwner;
  final int marketStatus;
  final int likeCount;
  NFTobject({required this.PRIMARYKEY,required this.address, required this.nID,required this.name,required this.description,required this.metaDataType,required this.dataLink,required this.collectionName,required this.creatorName,required this.currentOwner,required this.marketStatus, required this.likeCount});

  factory NFTobject.fromJson(Map<String, dynamic> json) {
    return NFTobject(
        PRIMARYKEY: json["id"],
        address: json['UID'],
        nID: json['index'],
        name: json['name'],
        description: json['description'],
        metaDataType: json['metaDataType'],
        dataLink: json['dataLink'],
        collectionName: json['collectionName'],
        creatorName: json['creator'],
        currentOwner: json['currentOwner'],
        marketStatus: json['marketStatus'],
        likeCount: json["numLikes"],
    );
  }

  Map<String, dynamic> toJson() => {
    'PRIMARYKEY' : PRIMARYKEY,
    'UID' : address,
    'nID': nID,
    'name': name,
    'description': description,
    'metaDataType':metaDataType,
    'dataLink': dataLink,
    'collectionName': collectionName,
    'creatorName': creatorName,
    'currentOwner': currentOwner,
    'marketStatus': marketStatus,
    "likeCount": likeCount
  };
}



class NFTcollectionobject {
  final String? name;
  final String? description;
  final String? collectionImage;
  final String? owner;
  final int? numLikes;
  final String? category;
  NFTcollectionobject({required this.name, required this.description,required this.collectionImage,required this.category,required this.numLikes,required this.owner});

  factory NFTcollectionobject.fromJson(Map<String, dynamic> json) {
    return NFTcollectionobject(
      name: json['name'],
      collectionImage: json['collectionImage'],
      description: json['description'],
      numLikes : json["numLikes"],
      owner : json["owner"],
      category : json["category"],
    );
  }

  Map<String, dynamic> toJson() => {
    'name': name,
    'description': description,
    'collectionImage':collectionImage,
    'numLikes': numLikes,
    'owner': owner,
    'category': category,
  };
}

class tokenObject {
  final String access;
  final String refresh;
  tokenObject({required this.access, required this.refresh});

  factory tokenObject.fromJson(Map<String, dynamic> json) {
    return tokenObject(
      refresh: json['refresh'],
      access: json['access'],
    );
  }

  Map<String, dynamic> toJson() => {
    'access': access,
    'refresh': refresh,
  };
}
class userObject {
  final String address;
  final String? name;
  final String? profilePicture;
  final String? mailAddress;

  userObject({required this.address,required this.name,required this.profilePicture,required this.mailAddress});

  factory userObject.fromJson(Map<String, dynamic> json) {
    return userObject(
      address: json['uAddress'],
      name: json['username'],
      profilePicture: json['profilePicture'],
      mailAddress: json['email'],
    );
  }

  Map<String, dynamic> toJson() => {
    'UID' : address,
    'name': name,
    "profilePicture": profilePicture,
    "mailAddress": mailAddress,
  };
}
Future<List<NFTobject>>getNFTs() async {
  var client = http.Client();

  try {
    final response = (await client.get(
        Uri.http('mertd.pythonanywhere.com', '/api/nfts')));

    final items = json.decode(utf8.decode(response.bodyBytes)).cast<Map<String, dynamic>>();

    List<NFTobject> nfts = items.map<NFTobject>((json) {
      return NFTobject.fromJson(json);
    }).toList();
    return nfts;

  } finally {
  client.close();
  }
}
Future<void>getUsers() async {

  var client = http.Client();

  try {
    final response = (await client.get(
        Uri.https('mertd.pythonanywhere.com', '/api/users')));

    final items = json.decode(utf8.decode(response.bodyBytes)).cast<Map<String, dynamic>>();

    List<userObject> users = items.map<userObject>((json) {
      return userObject.fromJson(json);
    }).toList();
    variables.userDatas_ = users;

  } finally {
    client.close();
  }


}
Future<void>getSpesificNFT(String nftAddress) async {
  var client = http.Client();

  try {
    final response = (await client.get(
        Uri.https('mertd.pythonanywhere.com', "/api/nfts" , {"UID": nftAddress})));


    final items = json.decode(utf8.decode(response.bodyBytes)).cast<Map<String, dynamic>>();

    List<NFTobject> users = items.map<NFTobject>((json) {
      return NFTobject.fromJson(json);
    }).toList();


  } finally {
    client.close();
  }
}
Future<void>getCategoryNames() async{

  var client = http.Client();

  try {

    final request = http.Request("GET", Uri.parse("https://mertd.pythonanywhere.com/api/categories/"));
    request.headers.addAll(<String, String>{
      "Accept": "application/json",
      "Content-Type": "application/json",
    });

    final response = await request.send();

    final items = json.decode(await response.stream.bytesToString()).cast<Map<String, dynamic>>();

    List<categories> cats = items.map<categories>((json) {
      return categories.fromJson(json);
    }).toList();
  print(cats);
    variables.allCategories = cats;
    print(variables.allCategories );
  } finally {
    client.close();
  }
}
Future<void>getCollections() async{

  var client = http.Client();

  try {

    final request = http.Request("GET", Uri.parse("https://mertd.pythonanywhere.com/api/nftcollections/"));
    request.headers.addAll(<String, String>{
      "Accept": "application/json",
      "Content-Type": "application/json",
    });

    final response = await request.send();

    final items = json.decode(await response.stream.bytesToString()).cast<Map<String, dynamic>>();

    List<NFTcollectionobject> cats = items.map<NFTcollectionobject>((json) {
      return NFTcollectionobject.fromJson(json);
    }).toList();
    print(cats);
    variables.allNFTcollections = cats;
    print(variables.allCategories );
  } finally {
    client.close();
  }
}

Future<void>getHotCollections() async{

  var client = http.Client();

  try {

    final request = http.Request("GET", Uri.parse("https://mertd.pythonanywhere.com/api/hottest/"));
    request.headers.addAll(<String, String>{
      "Accept": "application/json",
      "Content-Type": "application/json",
    });

    final response = await request.send();

    final items = json.decode(await response.stream.bytesToString()).cast<Map<String, dynamic>>();

    List<NFTcollectionobject> cats = items.map<NFTcollectionobject>((json) {
      return NFTcollectionobject.fromJson(json);
    }).toList();
    print(cats);
    variables.hotNFTCollections = cats;
    print(variables.allCategories );
  } finally {
    client.close();
  }
}

Future<void> initializeWeb3() async{
  const String rpcUrl = 'https://speedy-nodes-nyc.moralis.io/8c481143b2899787d54bcebd/avalanche/testnet';
  final client = Web3Client(rpcUrl, http.Client());
  variables.web3Client = client;
}

//LOADS MARKET CONTRACT !
Future<void> loadContract()async {
  String ABI = await rootBundle.loadString("assets/contractAbi.json");
  String contractAddress =  "0x85EE8C2B66D2659A94d820b63c8E8e376B4Fc7a3"; //DEPLOYMENT ADDRESS COMES HERE

  final contract = DeployedContract(ContractAbi.fromJson(ABI,"HW2Token"), EthereumAddress.fromHex(contractAddress));
  print("loadContract ran for $contractAddress");
  variables.nftMarketContract = contract;
}
//LOADS SU COIN CONTRACT !
Future<void> loadSUcoin() async{
  String ABI = await rootBundle.loadString("assets/suCoinAbi.json");
  var contractAddress = "0x748b33652d3dF39be54a1c3C378b7d9178D20543";
  final contract = DeployedContract(ContractAbi.fromJson(ABI,"HW2Token"), EthereumAddress.fromHex(contractAddress));
  print("loadContract ran for $contractAddress");
  variables.suCoinContract = contract;
}
//GET SPECIFIC PRICE OF NFT MEANING BID!, FROM MARKET CONTRACT; USING AVALANCHE NODES.

Future<BigInt> getSpecificPrice(String address, BigInt id) async {
  final ethFunction = variables.nftMarketContract.function("AuctionData");
  var x = await variables.web3Client.call(contract: variables.nftMarketContract, function: ethFunction, params: [EthereumAddress.fromHex(address),id,]);
  return x[0];
}
//GET USER BALANCE ___NOT ON MARKET___ todo

//GET USER BALANCE ON MARKET
Future<BigInt> getUserBalanceOnMarket(EthereumAddress address)async {
  final ethFunction = variables.nftMarketContract.function("UserData");
  var x = await variables.web3Client.call(contract: variables.nftMarketContract, function: ethFunction, params: [address,]);
  return x[0];
}
Future<BigInt> getUserBalanceNotOnMarket(EthereumAddress address) async{
  final ethFunction = variables.suCoinContract.function("balanceOf");
  var x = await variables.web3Client.call(contract: variables.suCoinContract, function: ethFunction, params: [address,]);
  return x[0];
}
Future<void> getUserOwnedNFTs() async{

  var client = http.Client();

  try {

    final response = (await client.get(
        Uri.https('mertd.pythonanywhere.com', "/api/nfts/" , {"currentOwner":"${variables.userDatas[0].name}","metaDataType":"image"})));


    final items  = json.decode(utf8.decode(response.bodyBytes)).cast<Map<String, dynamic>>();

    variables.userOwnedNFTS[0] = items.map<NFTobject>((json) {
      return NFTobject.fromJson(json);
    }).toList();


    final response2 = (await client.get(
        Uri.https('mertd.pythonanywhere.com', "/api/nfts/" , {"currentOwner":"${variables.userDatas[0].name}","metaDataType":"video"})));


    final items2  = json.decode(utf8.decode(response2.bodyBytes)).cast<Map<String, dynamic>>();


    variables.userOwnedNFTS[1] = items2.map<NFTobject>((json) {
      return NFTobject.fromJson(json);
    }).toList();

    final response3 = (await client.get(
        Uri.https('mertd.pythonanywhere.com', "/api/nfts/" , {"currentOwner":"${variables.userDatas[0].name}","metaDataType":"audio"})));


    final items3  = json.decode(utf8.decode(response3.bodyBytes)).cast<Map<String, dynamic>>();





    variables.userOwnedNFTS[2] = items3.map<NFTobject>((json) {
      return NFTobject.fromJson(json);
    }).toList();

  }
  catch(exception){
    print(exception);
  }
  finally {
    client.close();
  }
}

Future<void> getNftsByCategory() async{

  var client = http.Client();

  try {
    final response = (await client.get(
        Uri.https('mertd.pythonanywhere.com', "/api/nfts/" , {"metaDataType":"image"})));


    final items  = json.decode(utf8.decode(response.bodyBytes)).cast<Map<String, dynamic>>();


    variables.allNftsByCategory[0] = items.map<NFTobject>((json) {
      return NFTobject.fromJson(json);
    }).toList();

    final response2 = (await client.get(
        Uri.https('mertd.pythonanywhere.com', "/api/nfts/" , {"metaDataType":"video"})));


    final items2  = json.decode(utf8.decode(response2.bodyBytes)).cast<Map<String, dynamic>>();


    variables.allNftsByCategory[1] = items2.map<NFTobject>((json) {
      return NFTobject.fromJson(json);
    }).toList();

    final response3 = (await client.get(
        Uri.https('mertd.pythonanywhere.com', "/api/nfts/" , {"metaDataType":"audio"})));


    final items3  = json.decode(utf8.decode(response3.bodyBytes)).cast<Map<String, dynamic>>();


    variables.allNftsByCategory[2] = items3.map<NFTobject>((json) {
      return NFTobject.fromJson(json);
    }).toList();

  } finally {
    client.close();
  }
}

Future<List<NFTcollectionobject>> getFavoritedCollections() async{

  var client = http.Client();
  try{
    final response3 = (await client.get(
        Uri.https('mertd.pythonanywhere.com', "/api/watchLists/" ,{"user": "${variables.userDatas[0].address}"})));


    final x2  = json.decode(utf8.decode(response3.bodyBytes)).cast<Map<String, dynamic>>();


    print(x2);

    return x2.map<NFTcollectionobject>((json) {
      return NFTcollectionobject.fromJson(json);
    }).toList();
  }
  catch(exception){
    return [NFTcollectionobject(name: "Null", description: "Null", collectionImage: "Null", category: "Null", numLikes: 0, owner: "Null"),];
  }


}

Future<List<userObject>> getCollectionFavouriters(String name) async{

  var client = http.Client();
  try{
    final response3 = (await client.get(
        Uri.https('mertd.pythonanywhere.com', "/api/watchLists/" ,{"nftCollection": "$name"})));


    final x2  = json.decode(utf8.decode(response3.bodyBytes)).cast<Map<String, dynamic>>();


    print(x2);

    return x2.map<userObject>((json) {
      return userObject.fromJson(json);
    }).toList();
  }
  catch(exception){
    return [userObject(name: "Null", address: "Null", mailAddress: "Null", profilePicture: "Null")];
  }


}
void getFavoritedNFTs() async{
  var client = http.Client();

  try {
    final response3 = (await client.get(
        Uri.https('mertd.pythonanywhere.com', "/api/favorites/" ,{"user": "${variables.userDatas[0].address}"})));


    final x2  = json.decode(utf8.decode(response3.bodyBytes)).cast<Map<String, dynamic>>();

    print(x2);
    if(x2 != "[]"){
      final items = x2;

      variables.favorites = items.map<NFTobject>((json) {
        return NFTobject.fromJson(json);
      }).toList();

    }
    else{

    }




  } finally {
    client.close();
  }
}

Future<void> LoadEverything()async{
  await initializeWeb3();
  await loadContract();
  await loadSUcoin();
  await getUsers();
  await getCategoryNames();
  await getUserOwnedNFTs();
  await getNftsByCategory();
  await getCollections();
  //await getHotCollections();
  variables.userBalanceNotOnMarket = await getUserBalanceNotOnMarket(EthereumAddress.fromHex("0xDDcbd1913CA06fC43A32c5eFB82748301334bB7f"));
  var y = await getUserBalanceOnMarket(EthereumAddress.fromHex("0xDDcbd1913CA06fC43A32c5eFB82748301334bB7f")); //WORKED
  variables.userBalanceOnMarket = y;

}