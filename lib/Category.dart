import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:nft_market/nft_container_objects.dart';
import 'dart:math'; //used for the random number generator
import "dart:io";
import 'package:path_provider/path_provider.dart';
import "dart:convert";
import 'package:nft_market/Globals/Variables.dart' as variables;
import 'package:nft_market/nft_page.dart';
import 'package:nft_market/Search.dart';
import "package:nft_market/collection_object.dart";
class Category_page extends StatefulWidget {
  final int collection_id;
  const Category_page({Key? key, required this.collection_id}) : super(key: key);


  @override
  _Category_page createState() => _Category_page();


}




class _Category_page extends State<Category_page> {
  late double size;
  late String highlow;
  late String name_ascend;
  late String sorting;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    size = 400.0;
    name_ascend = "A->Z";
    highlow = "Lowest first";
    sorting = "0";
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: variables.backgroundColor,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerTop,
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FloatingActionButton.small(
                child: Icon(Icons.arrow_back_rounded, color: Colors.white,),
                backgroundColor: variables.backgroundColor,
                heroTag: "btn2",
                onPressed: ()=>{
                  Navigator.pop(context),
                }
            ),
            FloatingActionButton.small(
                child: Icon(CupertinoIcons.sort_down, color: Colors.white,),
                backgroundColor: variables.backgroundColor,
                heroTag: "btn1",
                onPressed: ()=>{
                  showDialog(context: context, builder: (context) {
                    return StatefulBuilder(builder: (context,setState){
                      return AlertDialog(
                        content: Container(
                          height: 100,
                          child: Column(

                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,


                                children: [
                                  TextButton(
                                    style: TextButton.styleFrom(
                                        padding: const EdgeInsets.all(6.0),
                                        primary: Colors.white,
                                        textStyle: const TextStyle(fontSize: 20),
                                        side: BorderSide(color: Colors.white)
                                    ),
                                    child: Text("Name", style: variables.assetFont,),onPressed: ()=>{
                                    setState(() => {
                                      if(name_ascend == "A->Z"){
                                        name_ascend = "Z->A",
                                        sorting = "1"
                                      }
                                      else{
                                        sorting = "0",
                                        name_ascend = "A->Z"
                                      }
                                    }
                                    ),

                                  },


                                  ),
                                  Text("  " +name_ascend, style: variables.assetFont,)


                                ],

                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,


                                children: [
                                  TextButton(style: TextButton.styleFrom(
                                      padding: const EdgeInsets.all(6.0),
                                      primary: Colors.white,
                                      textStyle: const TextStyle(fontSize: 20),
                                      side: BorderSide(color: Colors.white)
                                  ),child: Text("Like Count", style: variables.assetFont,),onPressed: () =>
                                      setState(() => {
                                        if(highlow == "Lowest first") {
                                          highlow = "Highest first",
                                          sorting = "3",
                                        }
                                        else{
                                          highlow = "Lowest first",
                                          sorting = "2",
                                        }
                                      }
                                      ),),


                                  Text("  " + highlow, style: variables.assetFont,),


                                ],

                              ),

                            ],
                          ),
                        ),
                        title: Text("Sort items", style: variables.titleFontBig),


                        backgroundColor: variables.buttonColor,

                      );
                    }
                    );

                  }
                  ),
                }
            ),
          ],
        ),
      ),

      body: NotificationListener<ScrollNotification>(
        onNotification: (scrollNotification) {
          setState(() {
            if (scrollNotification.metrics.pixels > 0) {

              if(400 -scrollNotification.metrics.pixels *1.5 < 100){
                size = 100;

              }

              else {
                size = 400 - scrollNotification.metrics.pixels * 1.5;

              }


            }
          });
          return true;
        },
        child: Column(
          children: [
            Container(
              width: variables.phoneSizeInfo.width,
              height: size*3/4,
              decoration: BoxDecoration(
                  color: variables.buttonColor,
                image: DecorationImage(image: NetworkImage(variables.allCategories[widget.collection_id].pic2 ?? "https://www.workland.com.tr/wp-content/uploads/2021/10/blockchain-art.jpeg")
                )
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 215-size/2, bottom: 10),
                    child: CircleAvatar(

                      radius: size/10 + 20,

                      backgroundImage: NetworkImage(variables.allCategories[widget.collection_id].pic1 ?? "https://i4.hurimg.com/i/hurriyet/75/0x0/616438084e3fe10910e48437.jpg"),
                      backgroundColor: Colors.black,
                    ),
                  ),






                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                padding: EdgeInsets.all(10),
                alignment: Alignment.topLeft,
                height: size / 4 + 15,
                child: Wrap(
                  direction: Axis.horizontal,

                  crossAxisAlignment: WrapCrossAlignment.start,
                  children: [
                    Text(variables.allCategories[widget.collection_id].name, style: variables.scrollViewFont), //BACKEND DATA
                    Container(height: 5,),
                    Text("Description of category", style: variables.titleFontSmall,),//BACKEND DATA
                    Container(height: 20,),
                    Text("Trending NFTs", style: variables.scrollViewFont), //BACKEND DATA
                  ],
                ),
              ),
            ),//DESC NAME ETC.
            Expanded(

              child: GridView.builder(

                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 0.7,
                ),
                physics: BouncingScrollPhysics(),

                  itemCount: variables.allNftsByCategory[widget.collection_id].length,
                  itemBuilder: (BuildContext context, int index) {
                    if (sorting == "0") {
                      var tempObject = variables.allNftsByCategory[widget.collection_id];
                      tempObject.sort((a, b) => a.name.toString().toLowerCase().compareTo(b.name.toString().toLowerCase()));
                      return Container(
                          child: nft_container_objects(data: tempObject[index])
                      );
                    }
                    else if (sorting == "1") {
                      var tempObject = variables.allNftsByCategory[widget.collection_id];
                      tempObject.sort((b, a) => a.name.toString().toLowerCase().compareTo(b.name.toString().toLowerCase()));
                      return Container(
                          child: nft_container_objects(data: tempObject[index])
                      );
                    }
                    else if (sorting == "2") {
                      var tempObject = variables.allNftsByCategory[widget.collection_id];
                      tempObject.sort((a, b) => a.likeCount.toString().toLowerCase().compareTo(b.likeCount.toString().toLowerCase()));
                      return Container(
                          child: nft_container_objects(data: tempObject[index])
                      );
                    }
                    else {
                      var tempObject = variables.allNftsByCategory[widget.collection_id];
                      tempObject.sort((b, a) => a.likeCount.toString().toLowerCase().compareTo(b.likeCount.toString().toLowerCase()));
                      return Container(
                          child: nft_container_objects(data: tempObject[index])
                      );
                    }
                  return Container(


                        child: nft_container_objects(data: variables.allNftsByCategory[widget.collection_id][index]));
                  }
              ),
            ),
          ],
        ),
      ),//BACKEND DATA NEEDED
    );
  }
}
