import 'package:flutter/material.dart';
import 'package:nft_market/Globals/backend_config.dart';
import 'package:nft_market/collection_object.dart';
import 'package:search_page/search_page.dart';
import 'package:nft_market/nft_container_objects.dart';
import 'package:nft_market/Globals/Variables.dart' as variables;
/// This is a very simple class, used to
/// demo the `SearchPage` package

class searching_page extends StatelessWidget {
  final String type;
  final List<userObject> obj;
  //BACKEND DATA
  searching_page(this.type, this.obj);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: variables.buttonColor,
        title: Text('$type'),
      ),
      body: Container(
        color: variables.backgroundColor,
        child: ListView.builder(

          itemCount: obj.length,
          itemBuilder: (context, index) {
            final userObject person = obj[index];
            return ListTile(
              leading: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.white),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: FadeInImage(image: NetworkImage(person.profilePicture ?? "https://upload.wikimedia.org/wikipedia/commons/f/f4/User_Avatar_2.png"


                  ),
                    imageErrorBuilder:
                        (context, error, stackTrace) {
                      return Image.network(
                          "https://upload.wikimedia.org/wikipedia/commons/f/f4/User_Avatar_2.png",
                          fit: BoxFit.fitWidth);
                    },
                    placeholder: AssetImage("assets/images/music.jpg"),
                  ),
                ),
              ),
              title: Text(person.name ?? "loading",style: variables.assetFont,),
              subtitle: Text(person.address,style: variables.collectionFont),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: variables.buttonColor,
        tooltip: 'Search people',
        onPressed: () => showSearch(
          context: context,
          delegate: SearchPage<userObject>(
            barTheme: ThemeData(textTheme: TextTheme(headline6:TextStyle(color: Colors.white,)),hintColor: Colors.white,appBarTheme: AppBarTheme(backgroundColor: variables.buttonColor),scaffoldBackgroundColor: variables.backgroundColor,textSelectionTheme: TextSelectionThemeData(selectionColor: Colors.white,cursorColor: Colors.white)),

            onQueryUpdate: (s) => print(s),
            items: obj,
            searchLabel: 'Search people',
            suggestion: Center(
              child: Text('Filter people by name, surname or age',style: variables.collectionFont,),
            ),
            failure: Center(
              child: Text('No person found :(', style: variables.collectionFont,),
            ),

            filter: (person) => [
              person.name
            ],
            builder: (person) => ListTile(
              leading: Image(image: NetworkImage(person.profilePicture ?? "https://upload.wikimedia.org/wikipedia/commons/f/f4/User_Avatar_2.png")),
              title: Text(person.name ?? "",style: variables.assetFont,),
              subtitle: Text(person.address,style: variables.collectionFont,),
            ),
          ),
        ),
        child: Icon(Icons.search),
      ),
    );
  }
}



class searching_page_NFT extends StatelessWidget {

  final List<NFTobject> datas;

  searching_page_NFT(this.datas);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: variables.buttonColor,
        title: Text('Search'),
      ),
      body: Container(
        color: variables.backgroundColor,
        child: ListView.builder(

          itemCount: this.datas.length,
          itemBuilder: (context, index) {
            final NFTobject person = this.datas[index];
            return Padding(
              padding: EdgeInsets.only(right: variables.phoneSizeInfo.width/11.25, left: variables.phoneSizeInfo.width/11.25),
              child: nft_container_objects(data: person), //todo make iterative
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: variables.buttonColor,
        tooltip: 'Search NFT\'s',
        onPressed: () => showSearch(

          context: context,
          delegate: SearchPage<NFTobject>(
            barTheme: ThemeData(
              textTheme: TextTheme(headline6:TextStyle(color: Colors.white,)),
              hintColor: Colors.white,appBarTheme: AppBarTheme(backgroundColor: variables.buttonColor),
              scaffoldBackgroundColor: variables.backgroundColor,
              textSelectionTheme: TextSelectionThemeData(selectionColor: Colors.white,cursorColor: Colors.white),

              floatingActionButtonTheme: FloatingActionButtonThemeData(backgroundColor: variables.backgroundColor),
            ),

            onQueryUpdate: (s) => print(s),
            items: this.datas,
            searchLabel: 'Search NFT\'s',
            suggestion: Center(
              child: Text('Filter NFT\'s by name',style: variables.collectionFont,),
            ),
            failure: Center(
              child: Text('No NFT\'s found :(', style: variables.collectionFont,),
            ),

            filter: (person) => [
              person.name,
            ],
            builder: (person) => Padding(
              padding: EdgeInsets.only(right: variables.phoneSizeInfo.width/11.25, left: variables.phoneSizeInfo.width/11.25),
              child: nft_container_objects(data: person),//todo make iterative
            ),


          ),

        ),

        child: Icon(Icons.search),
      ),
    );
  }
}


class searching_page_NFT_collections extends StatelessWidget {

  final List<NFTcollectionobject> datas;

  searching_page_NFT_collections(this.datas);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: variables.buttonColor,
        title: Text('Search'),
      ),
      body: Container(
        color: variables.backgroundColor,
        child: ListView.builder(

          itemCount: this.datas.length,
          itemBuilder: (context, index) {
            final NFTcollectionobject person = this.datas[index];
            return Padding(
              padding: EdgeInsets.only(right: variables.phoneSizeInfo.width/11.25, left: variables.phoneSizeInfo.width/11.25),
              child: collection_object( person), //todo make iterative
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: variables.buttonColor,
        tooltip: 'Search NFT\'s',
        onPressed: () => showSearch(

          context: context,
          delegate: SearchPage<NFTcollectionobject>(
            barTheme: ThemeData(
              textTheme: TextTheme(headline6:TextStyle(color: Colors.white,)),
              hintColor: Colors.white,appBarTheme: AppBarTheme(backgroundColor: variables.buttonColor),
              scaffoldBackgroundColor: variables.backgroundColor,
              textSelectionTheme: TextSelectionThemeData(selectionColor: Colors.white,cursorColor: Colors.white),

              floatingActionButtonTheme: FloatingActionButtonThemeData(backgroundColor: variables.backgroundColor),
            ),

            onQueryUpdate: (s) => print(s),
            items: this.datas,
            searchLabel: 'Search NFT\'s',
            suggestion: Center(
              child: Text('Filter NFT\'s by name',style: variables.collectionFont,),
            ),
            failure: Center(
              child: Text('No NFT\'s found :(', style: variables.collectionFont,),
            ),

            filter: (person) => [
              person.name,
            ],
            builder: (person) => Padding(
              padding: EdgeInsets.only(right: variables.phoneSizeInfo.width/11.25, left: variables.phoneSizeInfo.width/11.25),
              child: collection_object(person),//todo make iterative
            ),


          ),

        ),

        child: Icon(Icons.search),
      ),
    );
  }
}